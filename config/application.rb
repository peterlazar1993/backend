require_relative 'boot'
require "rails"
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
Bundler.require(*Rails.groups)
module BlacklotusAdmin
  class Application < Rails::Application
    config.assets.quiet = true
    config.generators do |generate|
      generate.helper false
      generate.javascripts false
      generate.request_specs false
      generate.routing_specs false
      generate.stylesheets false
      generate.test_framework :rspec
      generate.view_specs false
    end
    config.action_controller.action_on_unpermitted_parameters = :raise
    config.active_job.queue_adapter = :delayed_job
    config.api_only = false
    config.paperclip_defaults = {
      storage: :s3,
      preserve_files: true,
      s3_region: ENV["AWS_S3_REGION"],
      s3_host_alias: ENV["AWS_S3_HOST_ALIAS"],
      s3_protocol: :https,
      path: "/:class/:attachment/:id_:basename.:extension",
      url: ':s3_alias_url',
      s3_credentials: {
        bucket: ENV["AWS_S3_BUCKET"],
        # s3_host_name: "s3.amazonaws.com",
        access_key_id: ENV["AWS_ACCESS_KEY_ID"],
        secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]
      }
    }
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :put, :options]
      end
    end
    Paperclip::Attachment.default_options[:default_url] = ""
  end
  Raven.configure do |config|
    config.dsn = 'https://e46b2c0406f9486aaec410f341167510:16d5090dbe7c4f959286e645b82ca9a5@sentry.io/231726'
    config.environments = %w[ production ]
  end
end

APN_PUSH_APP = "bl_dev_apn_app"
GCM_PUSH_APP = "android_app"
