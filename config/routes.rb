Rails.application.routes.draw do

  mount_devise_token_auth_for 'User', at: 'auth', :controllers => {
    :registrations => "users/registrations",
    :sessions => "users/sessions",
    :passwords => "users/passwords"
  }, skip: [:omniauth_callbacks]

  devise_scope :user do
    post 'auth/social_auth', to: 'users/registrations#social_auth'
    post 'auth/social/sign_up', to: 'users/registrations#social_sign_up'
    post 'auth/social/sign_in', to: 'users/sessions#social_sign_in'
  end

  resources :videos, only: :index do
    post :favorite, on: :member
    post :unfavorite, on: :member
  end
  resources :video_categories, only: :index

  mount ActionCable.server => '/cable'

  namespace :admin do
    resources :categories
    resources :private_meditations
    resources :global_meditations
    resources :swaminars do
      get :export, on: :member
    end
    resources :quick_swaminars
    resources :questions
    resources :users
    # resources :global_meditation_participants
    # resources :users

    root to: "global_meditations#index"
  end

  resources :devices, only: :create
  resources :categories, only: :index
  resources :private_meditations, only: :index do
    post :favorite, on: :member
    post :unfavorite, on: :member
  end

  resources :global_meditations, only: :index do
    post :join, on: :member
    post :unjoin, on: :member
    get :attendees_by_location, on: :member
  end

  namespace :meditations do
    resources :sessions, only: :create
  end

  resources :swaminars, only: :index do
    post :join, on: :member
    post :unjoin, on: :member
    get :attendees_by_location

    resources :questions, only: %i[index create update] do
      post :upvote, on: :member
      post :undo_vote, on: :member
    end
  end

  resources :articles, only: [:index]
  resources :books, only: [:index]
  resources :events, only: [:index]
  resources :quotes, only: [:index]
  resources :galleries, only: [:index]
  resources :gallery_images, only: :index

  get "/users/stats", to: "users#stats"

  # This route should be removed once the client is migrated to use the new
  # /artices route instead of /posts
  get 'posts', to: 'articles#index'

end
