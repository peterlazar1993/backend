# Server Infrastructure
* We use AWS Elastic Beanstalk (EB) for hosting our backend app. It’s a PaaS like Heroku, except that we still have access to the underlying [EC2 instances](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features.managing.ec2.html), load balancing and auto scaling resources. AWS’s [developer guide](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/Welcome.html) is a good read.
* We have two environments - *staging* and *production* on EB.  Both have very similar configuration except that *staging* is configured to use cheaper / free hardware.
* For the database, we have two RDS instances running Postgres, one each for *staging* and *production*.  Again, the *staging* RDS instance uses cheaper / free  hardware.
* The EC2 instances are able to connect to the RDS instances because they have an inbound rule in their [security groups](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/AWSHowTo.RDS.html).
* Additional configuration for the EC2 instances are done using [ebextensions](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/ebextensions.html). Look at the `.ebextensions` directory in the project root to see what we’ve done.
* Scheduled jobs are run using Cron. Look at `.ebextensions/001_setup_cron.config` in the project root.
* The current version of the Rails app is deployed to  `/var/app/current/`.
* Logs are available in the usual `/var/logs`. The ones mostly of interest are `eb-activity.log`, `blacklotus_cron.log` and `puma/puma.log`.

### Adding a new user for SSH access 
* Generate your SSH key pair as follows
```
ssh-keygen -t rsa -b 4096 -C "YOUR_NAME@blacklotus" -f $HOME/.ssh/aws-eb
```
* Add your keys to the `.ebextensions/000_setup_additional_authorized_keys.config` in the project root. Look at the [last commit](https://gitlab.com/omswami-blacklotus/blacklotus-admin/commit/d2ea32f308d24a0923ff4874331fbda0635e83e7) which did the same thing.
* Remember to select this key when setting up [EB CLI](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3.html) on your machine.

### What to do if things go bad
Assuming the app is inaccessible and you are not able to SSH to the EC2 instances - 
1. First things first - you need access to the AWS console and probably the EB CLI as well.
2. Clone an existing environment - staging if possible, since staging might still be working fine.
3. Get the environment variables of the production environment using `eb printenv production`.
4. Set the same variables in the new environment using `eb setenv NAME_OF_NEW_ENVIRONMENT`. See [eb setenv - AWS Elastic Beanstalk](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb3-setenv.html) for more info.
5. Once the app is running in the new environment, you need to go the RDS console. Now add an inbound rule to the instance’s security group to accept connections from the new app environment’s security group.  This should let the EC2 instances of the new environment communicate with the RDS instances.
6. Once you have confirmed that the app is running, point the CNAME entry for *prod-native-app-server* to point to the new environment’s URL.
