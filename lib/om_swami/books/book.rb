# frozen_string_literal: true

require 'nokogiri'

module OmSwami
  module Books
    class Book
      def initialize(item_feed_object)
        @object = item_feed_object
      end

      def to_h
        {
          title: title,
          published_at: published_at,
          description: description,
          cover_image: cover_image,
          link: link
        }
      end

      private

        def title
          @object.title
        end

        def published_at
          @object.pubDate
        end

        def description
          paragraphs = html_tag_selector(@object.description, 'p')
          paragraphs.first.content.strip
        end

        def cover_image
          images = html_tag_selector(@object.content_encoded, 'img')
          images.first.attributes['src'].value unless images.empty?
        end

        def link
          @object.link
        end

        def html_tag_selector(section, selector)
          all_tags = Nokogiri::HTML(section)
          all_tags.css(selector)
        end
    end
  end
end
