# frozen_string_literal: true

require 'rss'
require 'om_swami/feed'
require 'om_swami/books/book'

module OmSwami
  module Books
    class Feed < ::OmSwami::Feed
      URL = 'http://omswami.com/book/feed'.freeze

      def initialize
        @url = URL
        @klass = ::OmSwami::Books::Book
      end

      # Note: Keeping fetch function indipendent of parent to consider
      # case of customized fetch for a feed. Incase at end of project
      # cycle the fetch is not cusotmized we will move it to parent.

      def fetch
        read_feed
        response = []
        get_items.each do |item|
          response << @klass.new(item).to_h
        end
        response
      end
    end
  end
end
