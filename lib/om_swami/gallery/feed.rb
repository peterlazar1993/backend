# frozen_string_literal: true

require 'rss'
require 'om_swami/feed'
require 'om_swami/gallery/gallery'

module OmSwami
  module Gallery
    class Feed < ::OmSwami::Feed
      URL = 'https://omswami.org/feed/?post_type=gallery'.freeze

      def initialize
        @url = URL
        @klass = ::OmSwami::Gallery::Gallery
      end

      # Note: Keeping fetch function indipendent of parent to consider
      # case of customized fetch for a feed. Incase at end of project
      # cycle the fetch is not cusotmized we will move it to parent.

      def fetch
        read_feed
        response = []
        get_items.each do |item|
          response << @klass.new(item).to_h
        end
        response
      end
    end
  end
end
