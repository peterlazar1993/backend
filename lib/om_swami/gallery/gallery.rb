# frozen_string_literal: true

require 'om_swami/gallery/images'

module OmSwami
  module Gallery
    class Gallery
      def initialize(item_feed_object)
        @object = item_feed_object
      end

      def to_h
        {
          name: name,
          date: date,
          link: gallery_link,
          cover_image: cover_image,
          images: gallery_images
        }
      end

      private

        def name
          @object.title
        end

        def date
          @object.pubDate
        end

        def gallery_link
          @object.link
        end

        def cover_image
          images = Nokogiri::HTML(@object.description).css('img')
          images.first.attributes['src'].value unless images.empty?
        end

        def gallery_images
          # do not proceed to the time consuming image scraping using nokogiri
          # if Gallery is already saved
          return [] if ::Gallery.exists?(name: name, date: date)
          Images.new(gallery_link).fetch_images
        end
    end
  end
end
