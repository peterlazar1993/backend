# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'

module OmSwami
  module Gallery
    class Images
      MAX_ATTEMPTS = 3
      def initialize(gallery_link)
        @url = gallery_link
        @attempt_count = 0
      end

      def fetch_images
        @attempt_count += 1
        images = gallery_images
        images.map { |img| GalleryImage.new(url: img.attr('src')) }
      rescue SocketError, Net::ReadTimeout => e
        puts "error: #{e}"
        sleep 3
        retry if @attempt_count < MAX_ATTEMPTS
      end

      private

        def gallery_images
          doc = Nokogiri::HTML(open(@url))
          doc.css('#gallery-container img')
        end
    end
  end
end
