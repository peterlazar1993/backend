# frozen_string_literal: true

require 'rss'
require 'open-uri'

module OmSwami
  class Feed
    def self.fetch
      new.fetch
    end

    def fetch
      raise 'Fetch not implmented in child class'
    end

    private

      def read_feed
        @feed ||= RSS::Parser.parse(@url)
      end

      def get_items
        @feed.items
      end
  end
end
