# frozen_string_literal: true

require 'nokogiri'
require 'uri'

module OmSwami
  module Blog
    class Article
      RESOLUTION = '1024'
      LENGTH_OF_SUMMARY = 29
      def initialize(item_feed_object)
        @object = item_feed_object
      end

      def to_h
        {
          title: title,
          published_at: published_at,
          tags: tags,
          cover_image: cover_image,
          link: link,
          summary: summary
        }
      end

      private

        def summary
          # Remove html tags
          description = @object.description.gsub(/<\/?[^>]*>/, '')
          # first n characters
          first_part = description[0..LENGTH_OF_SUMMARY]
          # all following characters until next fullstop
          second_part = if description[(LENGTH_OF_SUMMARY + 1)..-1].nil?
            ''
          else
            description[(LENGTH_OF_SUMMARY + 1)..-1].split('.')[0] + '.'
          end
          first_part + second_part
        end

        def title
          @object.title
        end

        def published_at
          @object.pubDate
        end

        def tags
          @object.categories.map(&:content).join(', ')
        end

        def cover_image
          image = nokogiri_page.css('img').try(:first)
          if image
            image_variants = image.attributes['srcset'].try(:value)
            if image_variants
              high_res = URI.extract(image_variants).select do |url|
                url.include? RESOLUTION
              end
              return high_res.first unless high_res.empty?
            end
            image.attributes['src'].value
          end
        end

        def link
          @object.link
        end

        def nokogiri_page
          @page ||= Nokogiri::HTML(@object.content_encoded)
        end
    end
  end
end
