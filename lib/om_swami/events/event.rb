# frozen_string_literal: true

require 'nokogiri'

module OmSwami
  module Events
    class Event
      def initialize(item_feed_object)
        @object = item_feed_object
      end

      def to_h
        {
          title: title,
          scheduled_at: scheduled_at,
          description: description,
          link: link
        }
      end

      private

        def title
          @object.title
        end

        def scheduled_at
          @object.pubDate.to_time.iso8601
        end

        def description
          Nokogiri::HTML.parse(@object.description).text
        end

        def link
          @object.link
        end
    end
  end
end
