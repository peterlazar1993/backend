# frozen_string_literal: true

module OmSwami
  module Quotes
    class Quote
      def initialize(item_feed_object)
        @object = item_feed_object
      end

      def to_h
        {
          content: content,
          date: Date.current
        }
      end

      private

        def content
          @object.link
        end
    end
  end
end
