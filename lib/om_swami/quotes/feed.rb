# frozen_string_literal: true

require 'rss'
require 'om_swami/feed'
require 'om_swami/quotes/quote'

module OmSwami
  module Quotes
    class Feed < ::OmSwami::Feed
      URL = 'http://omswami.com/feed/feedquote'.freeze

      def initialize
        @url = URL
        @klass = ::OmSwami::Quotes::Quote
      end


      # fetch logic is changed for quotes
      # This is due to quotes feed provide daily quote as a <link> tag
      # nested inside <channel>
      def fetch
        read_feed
        @klass.new(@feed.channel).to_h
      end

      private

        # overriding parent's read_feed method to pass false as a second
        # argument to RSS::Parser.parse to set RSS validations false
        # This is necessary beacuse quotes feed provides daily quote as a
        # <link> tag nested inside <channel> which will raise
        # RSS::MissingTagError: tag <description> is missing in tag <channel>
        def read_feed
          @feed ||= RSS::Parser.parse(@url, false)
        end
    end
  end
end
