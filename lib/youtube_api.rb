# frozen_string_literal: true

require 'youtube_api/o_auth_client'

module YoutubeApi
  def self.authorize_via_google_oauth_2
    oauth_client = OAuthClient.new
    oauth_client.authorize
  end
end
