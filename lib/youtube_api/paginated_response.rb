module YoutubeApi
  class PaginatedResponse
    def self.from_json(json)
      response = new
      response.instance_variable_set('@response_hash', JSON.parse(json))
      response
    end

    def last_page?
      @response_hash['nextPageToken'].nil?
    end

    def next_page_token
      @response_hash['nextPageToken']
    end

    def video_hashes
      @response_hash['items'].map do |item|
        {
          youtube_video_id: item['id']['videoId'],
          title: item['snippet']['title'],
          description: item['snippet']['description'],
          thumbnail_url: item['snippet']['thumbnails']['high']['url'],
          published_at: item['snippet']['publishedAt']
        }
      end
    end
  end
end
