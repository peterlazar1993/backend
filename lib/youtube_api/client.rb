require_relative 'paginated_response'

module YoutubeApi
  class Client
    MAX_RESULTS_PER_PAGE = 50

    def initialize(api_key:)
      @api_key = api_key
    end

    def get_videos(channel_id:, page_token: '')
      constant_params = { part: 'snippet', order: 'date', type: 'video' }
      params = constant_params.merge(
        maxResults: MAX_RESULTS_PER_PAGE,
        pageToken: page_token,
        channelId: channel_id,
        key: @api_key
      )
      response = RestClient.get(
        'https://www.googleapis.com/youtube/v3/search',
        params: params
      )
      PaginatedResponse.from_json response
    end
  end
end
