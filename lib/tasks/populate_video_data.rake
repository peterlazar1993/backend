namespace :db do
  desc 'Populate videos table with data from Youtube'
  task populate_videos_from_youtube: :environment do
    PopulateYoutubeDataJob.new.run
    puts 'Finished populating video data from Youtube'
  end
end
