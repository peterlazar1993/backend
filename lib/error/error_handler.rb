module Error
  require 'error/custom_error'
  require 'error/helpers/render'

  module ErrorHandler
    def self.included(clazz)
      clazz.class_eval do
        rescue_from StandardError do |e|
          Rails.logger.error e.to_s
          Rails.logger.error e.backtrace.join("\n")
          respond_with_error(:standard_error, 500, e.to_s)
        end

        rescue_from Error::CustomError do |e|
          respond_with_error(e.error, e.status, e.message.to_s)
        end

        rescue_from ActiveRecord::RecordNotFound do |e|
          respond_with_error(:record_not_found, 404, e.to_s)
        end

        rescue_from Pundit::NotAuthorizedError do |e|
          respond_with_error(:authorization_error, 403, pundit_error_message)
        end
      end
    end

    private

      def respond_with_error(_error, _status, _message)
        json = Error::Helpers::Render.json(_error, _status, _message)
        render json: json, status: _status
      end

      def pundit_error_message
        I18n.t('errors.pundit_errror')
      end
  end
end
