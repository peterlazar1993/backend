# frozen_string_literal: true

module Service
  extend ActiveSupport::Concern
  class_methods do
    def execute(*args)
      new(*args).execute
    end
  end

  private

    def notify_error(error)
      ErrorNotificationMailer.notify(
        self.class.to_s, error
      ).deliver_later
    end
end
