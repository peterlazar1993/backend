source "https://rubygems.org"

ruby "2.4.3"

gem 'rack-cors', :require => 'rack/cors'
gem 'acts_as_votable', '~> 0.11.1'
gem "autoprefixer-rails"
gem "delayed_job_active_record"
gem "fcm"
gem "flutie"
gem 'google-api-client', '~> 0.11'
gem "honeybadger"
gem "jquery-rails"
gem "launchy"
gem "normalize-rails", "~> 3.0.0"
gem 'obscenity'
gem "pg"
gem 'redis', '~> 3.0'
gem "puma"
# authorization using POROs
gem "pundit"
gem "rack-canonical-host"
gem "rails", "~> 5.1.0"
gem "recipient_interceptor"
gem "sass-rails", "~> 5.0"
gem "simple_form"
gem "sprockets", ">= 3.0.0"
gem "title"
gem "uglifier"
gem "administrate"
gem 'administrate-field-paperclip'
gem "paperclip", "~> 5.2.0"
gem "aws-sdk","~> 2"
gem 'ruby-mp3info', :require => 'mp3info'
gem 'active_model_serializers', '~> 0.10.0'
gem 'geocoder'
gem "sentry-raven"
gem 'devise_token_auth'
gem 'rest-client', '~> 2.0', '>= 2.0.2'
gem 'kaminari' #The administrate gem already uses this
gem 'api-pagination'
gem 'rubocop', '>= 0.47', require: false
gem 'health_check'

group :development do
  gem "listen"
  gem "spring"
  gem "spring-commands-rspec"
  gem "web-console"
  gem 'guard'
  gem 'guard-rspec', '~> 4.7', '>= 4.7.3'
end

group :development, :test do
  gem "awesome_print"
  gem "bullet"
  gem "bundler-audit", ">= 0.5.0", require: false
  gem "dotenv-rails"
  gem "factory_bot_rails"
  gem "pry-byebug"
  gem "pry-rails"
  gem "rspec-rails", "~> 3.5"
  gem 'foreman'
end

group :development, :staging do
  gem "rack-mini-profiler", require: false
end

group :test do
  gem "capybara"
  gem "capybara-webkit"
  gem "database_cleaner"
  gem "formulaic"
  gem "shoulda-matchers"
  gem "simplecov", require: false
  gem "timecop"
  gem "webmock"
  gem 'rspec-activemodel-mocks'
end

group :staging, :production do
  gem "rack-timeout"
end

group :production do
  gem 'appsignal'
  gem "skylight"
end
