class AddTrackToGlobalMeditations < ActiveRecord::Migration[5.0]
  def up
    add_attachment :global_meditations, :track
  end

  def down
    remove_attachment :global_meditations, :track
  end
end
