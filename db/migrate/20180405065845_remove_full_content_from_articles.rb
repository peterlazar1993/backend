class RemoveFullContentFromArticles < ActiveRecord::Migration[5.1]
  def change
    remove_column :articles, :full_content
  end
end
