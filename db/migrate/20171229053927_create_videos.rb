class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
      t.text :title
      t.text :description
      t.text :youtube_video_id
      t.text :thumbnail_url
      t.timestamp :published_at
      t.belongs_to :video_category, foreign_key: true

      t.timestamps
    end
  end
end
