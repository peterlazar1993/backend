class AddTrackDescriptionsToPrivateMeditation < ActiveRecord::Migration[5.0]
  def change
    add_column :private_meditations, :guided_track_description, :text
    add_column :private_meditations, :music_track_description, :text
  end
end
