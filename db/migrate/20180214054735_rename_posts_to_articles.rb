class RenamePostsToArticles < ActiveRecord::Migration[5.0]
  def change
    rename_table :posts, :articles
  end
end
