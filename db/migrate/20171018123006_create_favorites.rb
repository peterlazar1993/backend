class CreateFavorites < ActiveRecord::Migration[5.0]
  def change
    create_table :favorites do |t|
      t.references :user, foreign_key: true
      t.references :favorable
      t.string :favorable_type
      t.timestamps
    end
  end
end
