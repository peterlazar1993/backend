class CreateVideoCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :video_categories do |t|
      t.text :name
      t.text :description
      t.text :youtube_channel_id

      t.timestamps
    end
  end
end
