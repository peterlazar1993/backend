class AddLatitudeLongitudeCountryIpAddressToGlobalMeditationParticipant < ActiveRecord::Migration[5.0]
  def change
    add_column :global_meditation_participants, :latitude, :float
    add_column :global_meditation_participants, :longitude, :float
    add_column :global_meditation_participants, :country, :string
    add_column :global_meditation_participants, :ip_address, :string
  end
end
