class CreateSwaminars < ActiveRecord::Migration[5.0]
  def change
    create_table :swaminars do |t|
      t.string :broadcast_slug
      t.integer :attendees_count, default: 0
      t.integer :questions_count, default: 0

      t.timestamps
    end
  end
end
