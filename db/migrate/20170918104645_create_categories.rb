class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :count_of_meditations

      t.timestamps
    end
  end
end
