class CreateSwaminarAttendees < ActiveRecord::Migration[5.0]
  def change
    create_table :swaminar_attendees do |t|
      t.references :swaminar, foreign_key: true
      t.references :user, foreign_key: true
      t.float :latitude
      t.float :longitude
      t.string :country
      t.string :ip_address
      t.hstore :location

      t.timestamps
    end
  end
end
