class CreateEvent < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title, index: true
      t.string :published_at, index: true
      t.text :description
      t.string :link

      t.timestamps
    end
  end
end
