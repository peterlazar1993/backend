class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string :title, index: true
      t.datetime :published_at, index: true
      t.text :summary
      t.text :full_content
      t.string :cover_image
      t.string :link
      t.string :tags

      t.timestamps
    end
  end
end
