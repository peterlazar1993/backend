class CreateQuote < ActiveRecord::Migration[5.0]
  def change
    create_table :quotes do |t|
      t.text :content
      t.date :date
    end
  end
end
