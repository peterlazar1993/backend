class CreateGalleryImage < ActiveRecord::Migration[5.0]
  def change
    create_table :gallery_images do |t|
      t.belongs_to :gallery, index: true
      t.string :url

      t.timestamps
    end
  end
end
