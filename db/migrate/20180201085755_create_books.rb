class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :title, index: true
      t.datetime :published_at, index: true
      t.text :description
      t.string :cover_image
      t.string :link

      t.timestamps
    end
  end
end
