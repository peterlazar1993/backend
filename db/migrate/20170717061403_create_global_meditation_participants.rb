class CreateGlobalMeditationParticipants < ActiveRecord::Migration[5.0]
  def change
    create_table :global_meditation_participants do |t|
      t.references :global_meditation, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
