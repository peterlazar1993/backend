class CreateGlobalMeditations < ActiveRecord::Migration[5.0]
  def change
    create_table :global_meditations do |t|
      t.datetime :scheduled_at
      t.float :duration_in_minutes
      t.string :audio_url

      t.timestamps
    end
  end
end
