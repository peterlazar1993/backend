class DeleteCountryFromGlobalMeditationParticipants < ActiveRecord::Migration[5.0]
  def change
    remove_column :global_meditation_participants, :country
  end
end
