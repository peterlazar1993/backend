class CreateGallery < ActiveRecord::Migration[5.0]
  def change
    create_table :galleries do |t|
      t.string :name, index: true
      t.datetime :date, index: true
      t.string :link
      t.string :cover_image

      t.timestamps
    end
  end
end
