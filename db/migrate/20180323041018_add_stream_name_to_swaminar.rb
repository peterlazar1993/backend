class AddStreamNameToSwaminar < ActiveRecord::Migration[5.1]
  def change
    add_column :swaminars, :stream_id, :string
  end
end
