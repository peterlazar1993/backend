class AddLocationToGlobalMeditationParticipants < ActiveRecord::Migration[5.0]
  def change
    enable_extension "hstore"
    add_column :global_meditation_participants, :location, :hstore
  end
end
