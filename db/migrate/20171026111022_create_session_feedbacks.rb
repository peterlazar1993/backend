class CreateSessionFeedbacks < ActiveRecord::Migration[5.0]
  def change
    enable_extension "hstore"
    create_table :session_feedback do |t|
      t.references :session, foreign_key: true, index: true
      t.hstore :ratings
      t.text :notes

      t.timestamps
    end
  end
end
