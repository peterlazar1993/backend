class ChangeColumnNameOnEvents < ActiveRecord::Migration[5.1]
  def change
    rename_column :events, :published_at, :scheduled_at
  end
end
