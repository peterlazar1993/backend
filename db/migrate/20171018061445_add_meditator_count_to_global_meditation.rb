class AddMeditatorCountToGlobalMeditation < ActiveRecord::Migration[5.0]
  def change
    add_column :global_meditations, :joined_meditator_count, :integer
  end
end
