class DropAudioUrlFromGlobalMeditations < ActiveRecord::Migration[5.0]
  def change
    remove_column :global_meditations, :audio_url, :string
  end
end
