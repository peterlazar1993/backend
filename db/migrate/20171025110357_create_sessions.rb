class CreateSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :sessions do |t|
      t.references :user, foreign_key: true
      t.datetime :played_at
      t.integer :meditation_type
      t.integer :meditation_type_id
      t.float :duration_in_minutes

      t.timestamps
    end
  end
end
