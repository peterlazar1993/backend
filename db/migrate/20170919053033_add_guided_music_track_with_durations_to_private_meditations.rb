class AddGuidedMusicTrackWithDurationsToPrivateMeditations < ActiveRecord::Migration[5.0]
  def self.up
    change_table :private_meditations do |t|
      t.attachment :guided_track
      t.attachment :music_track
      t.column :guided_track_duration_in_minutes, :float
      t.column :music_track_duration_in_minutes, :float
    end
  end

  def self.down
    remove_attachment :private_meditations, :guided_track
    remove_attachment :private_meditations, :music_track
    remove_column :private_meditations, :guided_track_duration_in_minutes
    remove_column :private_meditations, :music_track_duration_in_minutes
  end
end
