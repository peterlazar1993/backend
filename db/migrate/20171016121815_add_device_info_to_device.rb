class AddDeviceInfoToDevice < ActiveRecord::Migration[5.0]
  def change
    remove_column :devices, :token
    add_column :devices, :device_id, :string
    add_column :devices, :platform, :string
  end
end
