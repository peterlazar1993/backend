class CreatePrivateMeditations < ActiveRecord::Migration[5.0]
  def change
    create_table :private_meditations do |t|
      t.string :name
      t.float :duration_in_minutes
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
