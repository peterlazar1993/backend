# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180423064118) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "articles", id: :serial, force: :cascade do |t|
    t.string "title"
    t.datetime "published_at"
    t.text "summary"
    t.string "cover_image"
    t.string "link"
    t.string "tags"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["published_at"], name: "index_articles_on_published_at"
    t.index ["title"], name: "index_articles_on_title"
  end

  create_table "books", id: :serial, force: :cascade do |t|
    t.string "title"
    t.datetime "published_at"
    t.text "description"
    t.string "cover_image"
    t.string "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["published_at"], name: "index_books_on_published_at"
    t.index ["title"], name: "index_books_on_title"
  end

  create_table "categories", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "count_of_meditations"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delayed_jobs", id: :serial, force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "devices", id: :serial, force: :cascade do |t|
    t.string "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "device_id"
    t.string "platform"
    t.integer "user_id"
    t.string "push_token"
    t.index ["user_id"], name: "index_devices_on_user_id"
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "scheduled_at"
    t.text "description"
    t.string "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["scheduled_at"], name: "index_events_on_scheduled_at"
    t.index ["title"], name: "index_events_on_title"
  end

  create_table "favorites", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "favorable_id"
    t.string "favorable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["favorable_id"], name: "index_favorites_on_favorable_id"
    t.index ["user_id"], name: "index_favorites_on_user_id"
  end

  create_table "galleries", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "date"
    t.string "link"
    t.string "cover_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["date"], name: "index_galleries_on_date"
    t.index ["name"], name: "index_galleries_on_name"
  end

  create_table "gallery_images", id: :serial, force: :cascade do |t|
    t.integer "gallery_id"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gallery_id"], name: "index_gallery_images_on_gallery_id"
  end

  create_table "global_meditation_participants", id: :serial, force: :cascade do |t|
    t.integer "global_meditation_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "latitude"
    t.float "longitude"
    t.string "ip_address"
    t.hstore "location"
    t.index ["global_meditation_id"], name: "index_global_meditation_participants_on_global_meditation_id"
    t.index ["user_id"], name: "index_global_meditation_participants_on_user_id"
  end

  create_table "global_meditations", id: :serial, force: :cascade do |t|
    t.datetime "scheduled_at"
    t.float "duration_in_minutes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "joined_meditator_count"
    t.string "track_file_name"
    t.string "track_content_type"
    t.integer "track_file_size"
    t.datetime "track_updated_at"
  end

  create_table "private_meditations", id: :serial, force: :cascade do |t|
    t.string "name"
    t.float "duration_in_minutes"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "guided_track_file_name"
    t.string "guided_track_content_type"
    t.integer "guided_track_file_size"
    t.datetime "guided_track_updated_at"
    t.string "music_track_file_name"
    t.string "music_track_content_type"
    t.integer "music_track_file_size"
    t.datetime "music_track_updated_at"
    t.float "guided_track_duration_in_minutes"
    t.float "music_track_duration_in_minutes"
    t.text "guided_track_description"
    t.text "music_track_description"
    t.index ["category_id"], name: "index_private_meditations_on_category_id"
  end

  create_table "questions", id: :serial, force: :cascade do |t|
    t.text "body"
    t.integer "user_id"
    t.string "askable_type"
    t.integer "askable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["askable_type", "askable_id"], name: "index_questions_on_askable_type_and_askable_id"
    t.index ["user_id"], name: "index_questions_on_user_id"
  end

  create_table "quotes", id: :serial, force: :cascade do |t|
    t.text "content"
    t.date "date"
  end

  create_table "session_feedback", id: :serial, force: :cascade do |t|
    t.integer "session_id"
    t.hstore "ratings"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_session_feedback_on_session_id"
  end

  create_table "sessions", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.datetime "played_at"
    t.integer "meditation_type"
    t.integer "meditation_type_id"
    t.float "duration_in_minutes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "swaminar_attendees", id: :serial, force: :cascade do |t|
    t.integer "swaminar_id"
    t.integer "user_id"
    t.float "latitude"
    t.float "longitude"
    t.string "country"
    t.string "ip_address"
    t.hstore "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["swaminar_id"], name: "index_swaminar_attendees_on_swaminar_id"
    t.index ["user_id"], name: "index_swaminar_attendees_on_user_id"
  end

  create_table "swaminars", id: :serial, force: :cascade do |t|
    t.string "broadcast_slug"
    t.integer "attendees_count", default: 0
    t.integer "questions_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "stream_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "unique_id"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.boolean "blacklisted", default: false
    t.string "city"
    t.integer "gender", default: 0
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "video_categories", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "description"
    t.text "youtube_channel_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "videos", id: :serial, force: :cascade do |t|
    t.text "title"
    t.text "description"
    t.text "youtube_video_id"
    t.text "thumbnail_url"
    t.datetime "published_at"
    t.integer "video_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["video_category_id"], name: "index_videos_on_video_category_id"
  end

  create_table "votes", id: :serial, force: :cascade do |t|
    t.string "votable_type"
    t.integer "votable_id"
    t.string "voter_type"
    t.integer "voter_id"
    t.boolean "vote_flag"
    t.string "vote_scope"
    t.integer "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
    t.index ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"
  end

  add_foreign_key "devices", "users"
  add_foreign_key "favorites", "users"
  add_foreign_key "global_meditation_participants", "global_meditations"
  add_foreign_key "global_meditation_participants", "users"
  add_foreign_key "private_meditations", "categories"
  add_foreign_key "session_feedback", "sessions"
  add_foreign_key "sessions", "users"
  add_foreign_key "swaminar_attendees", "swaminars"
  add_foreign_key "swaminar_attendees", "users"
  add_foreign_key "videos", "video_categories"
end
