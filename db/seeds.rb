# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

OM_SWAMI_YOUTUBE_CHANNEL_ID = "UCfqYeqSPekWE6tQEZHTnx1w".freeze
BLACK_LOTUS_YOUTUBE_CHANNEL_ID = "UC4V_pr7VZ9jViKDjUSUYOpA".freeze
VideoCategory.create(
  [
    { name: "Discourses", description: "Discourses", youtube_channel_id: OM_SWAMI_YOUTUBE_CHANNEL_ID },
    { name: "Meditation", description: "Meditation", youtube_channel_id: BLACK_LOTUS_YOUTUBE_CHANNEL_ID },
  ]
)
