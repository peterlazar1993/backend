class GlobalMeditationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "global_meditations-#{params[:id]}"
  end

  def unsubscribed
  end
end
