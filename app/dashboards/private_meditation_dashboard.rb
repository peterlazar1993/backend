require "administrate/base_dashboard"

class PrivateMeditationDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    category: Field::BelongsTo,
    id: Field::Number,
    name: Field::Text,
    guided_track: Field::Paperclip.with_options(url_only: true),
    guided_track_description: Field::Text,
    music_track: Field::Paperclip.with_options(url_only: true),
    music_track_description: Field::Text,
    music_track_duration: Field::Number.with_options(decimals: 2),
    guided_track_duration: Field::Number.with_options(decimals: 2),
    duration_in_minutes: Field::Number.with_options(decimals: 2),
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :category,
    :id,
    :name,
    :guided_track,
    :music_track,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :category,
    :id,
    :name,
    :guided_track,
    :guided_track_description,
    :music_track,
    :music_track_description,
    :duration_in_minutes,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :category,
    :name,
    :guided_track,
    :guided_track_description,
    :music_track,
    :music_track_description,
  ].freeze

  # Overwrite this method to customize how private meditations are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(private_meditation)
  #   "PrivateMeditation ##{private_meditation.id}"
  # end
end
