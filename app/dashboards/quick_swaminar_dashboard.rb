require "administrate/base_dashboard"

class QuickSwaminarDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    broadcast_slug: Field::String,
    attendees_count: Field::Number,
    questions_count: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    stream_id: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :broadcast_slug,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :broadcast_slug,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :broadcast_slug,
  ].freeze

  # Overwrite this method to customize how quick swaminars are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(quick_swaminar)
  #   "QuickSwaminar ##{quick_swaminar.id}"
  # end
end
