require "administrate/base_dashboard"

class GlobalMeditationDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    scheduled_at: Field::DateTime,
    duration_in_minutes: Field::Number.with_options(decimals: 2),
    track: Field::Paperclip.with_options(url_only: true),
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    joined_meditator_count: Field::Number
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :scheduled_at,
    :duration_in_minutes,
    :track,
    :joined_meditator_count
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :scheduled_at,
    :duration_in_minutes,
    :track,
    :updated_at,
    :joined_meditator_count
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :scheduled_at,
    :track,
  ].freeze

  # Overwrite this method to customize how global meditations are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(global_meditation)
  #   "GlobalMeditation ##{global_meditation.id}"
  # end
end
