require "administrate/base_dashboard"

class SwaminarDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    questions: Field::HasMany.with_options(limit: 15),
    id: Field::Number,
    title: Field::String,
    stream_name: Field::String,
    stream_title: Field::String,
    description: Field::Text,
    scheduled_at: Field::DateTime,
    broadcast_link: Field::String,
    attendees_count: Field::Number,
    questions_count: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    broadcast_slug: Field::String
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :title,
    :description,
    :broadcast_link,
    :broadcast_slug,
    :scheduled_at,
    :stream_name,
    :attendees_count,
    :questions_count
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :title,
    :description,
    :scheduled_at,
    :broadcast_link,
    :broadcast_slug,
    :stream_name,
    :stream_title,
    :attendees_count,
    :questions_count,
    :created_at,
    :updated_at,
    :questions
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :title,
    :description,
    :stream_title,
    :scheduled_at,
    :broadcast_slug
  ].freeze

  # Overwrite this method to customize how swaminars are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(swaminar)
    "Swaminar- #{swaminar.title}"
  end
end
