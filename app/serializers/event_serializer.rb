class EventSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :link,
             :scheduled_at,
             :description,
             :created_at
end
