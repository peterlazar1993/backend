class UserSerializer < ActiveModel::Serializer
  has_many :sessions
  attributes :session_stats

  def session_stats
      current_streak.attributes.except("id")
        .merge(longest_streak.attributes.except("id"))
        .merge(stats.attributes.except("id"))
        .merge(total_days_meditated: object.total_days_meditated)
        .merge(consistency: object.consistency)
  end

  def current_streak
    Session.current_streak_in_days(object) || CurrentStreak.new(0)
  end

  def longest_streak
    Session.longest_streak_in_days(object) || LongestStreak.new(0)
  end

  def stats
    Session.stats(object)
  end

  CurrentStreak = Struct.new(:current_streak) do
    def attributes
      {"current_streak": 0}
    end
  end

  LongestStreak = Struct.new(:longest_streak) do
    def attributes
      {"longest_streak_in_days": 0, "longest_streak_start_date": "", "longest_streak_end_date": ""}
    end
  end
end
