# frozen_string_literal: true

class SwaminarSerializer < ActiveModel::Serializer
  attributes :id,
    :title,
    :description,
    :scheduled_at,
    :broadcast_link,
    :attendees_count,
    :questions_count,
    :created_at
end
