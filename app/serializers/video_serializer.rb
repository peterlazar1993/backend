class VideoSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :youtube_video_id,
  :thumbnail_url, :published_at, :favorites_count,
  :is_favorited

  def is_favorited
    video = object
    current_user = scope || User.new
    video.favorited_by_user? current_user
  end
end
