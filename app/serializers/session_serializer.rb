class SessionSerializer < ActiveModel::Serializer

  attributes :id,
    :duration_in_minutes,
    :meditation_type,
    :meditation_type_id,
    :played_at,
    :feedback

  def feedback
    if object.feedback
      SessionFeedbackSerializer.new(object.feedback)
    end
  end
end
