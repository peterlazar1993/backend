class SessionFeedbackSerializer < ActiveModel::Serializer
  attributes :id,
    :notes,
    :ratings
end
