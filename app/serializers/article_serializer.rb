class ArticleSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :link,
             :published_at,
             :cover_image,
             :summary
end
