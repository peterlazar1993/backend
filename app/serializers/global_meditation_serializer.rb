class GlobalMeditationSerializer < ActiveModel::Serializer
  attributes :id,
    :audio_url,
    :duration_in_minutes,
    :attendees_count,
    :scheduled_at,
    :created_at,
    :updated_at

    def audio_url
      object.track.try(:url)
    end

    def attendees_count
      object.meditators.count
    end
end
