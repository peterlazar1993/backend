class BookSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :published_at,
             :cover_image,
             :link,
             :created_at
end
