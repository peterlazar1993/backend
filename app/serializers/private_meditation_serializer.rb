class PrivateMeditationSerializer < ActiveModel::Serializer
  attributes :id,
    :category_id,
    :name,
    :guided_track_url,:music_track_url,
    :guided_track_duration_in_minutes,
    :music_track_duration_in_minutes,
    :guided_track_description,
    :music_track_description,
    :duration_in_minutes,
    :created_at,
    :favorites_count,
    :is_favorited

  def is_favorited
    private_meditation = object
    current_user = scope || User.new
    private_meditation.favorited_by_user? current_user
  end

  def guided_track_url
    object.guided_track.try(:url)
  end

  def music_track_url
    object.music_track.try(:url)
  end

  def favorites_count
    object.favorites.count
  end
end
