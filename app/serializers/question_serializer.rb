# frozen_string_literal: true

class QuestionSerializer < ActiveModel::Serializer
  attributes :id,
    :body,
    :asked_by,
    :votes_count

  def votes_count
    object.get_upvotes.size
  end
end
