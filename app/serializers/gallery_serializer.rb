class GallerySerializer < ActiveModel::Serializer
  attributes :id,
  :name,
  :date,
  :link,
  :cover_image,
  :album_size

  def album_size
    object.gallery_images.count
  end
end
