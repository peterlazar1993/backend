class ErrorNotificationMailer < ApplicationMailer
  default from: 'notifications@example.com',
          to:   'vaidyanathan.b@multunus.com',
          cc:   %w(leena.sn@multunus.com peterlazar1993@gmail.com)

  def notify(klass, error)
    @error_notification = true
    @klass = klass
    @error = error
    mail(subject: 'Alert - An error occured in production')
  end
end
