# frozen_string_literal: true

require 'service'
require 'youtube_api'
require 'google/apis/youtube_v3'


class UpdateLiveStream
  include Service

  YT = Google::Apis::YoutubeV3
  CDN_FORMAT = '1080p'
  CDN_INGESTION_TYPE = 'rtmp'

  def initialize(id, title, description = nil)
    @client = YT::YouTubeService.new
    @client.authorization = YoutubeApi.authorize_via_google_oauth_2
    @id = id
    @title = title
    @description = description
  end

  def execute
    @client.update_live_stream(part, live_stream_object, {})
  end

  private

    def part
      'id,snippet,cdn,status'
    end

    def live_stream_object
      Google::Apis::YoutubeV3::LiveStream.new(
        id: @id,
        snippet: {
          title: @title,
          description: @description,
        },
        cdn: {
          # format: CDN_FORMAT,
          ingestion_type: CDN_INGESTION_TYPE
        }
      )
    end
end
