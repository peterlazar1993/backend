# frozen_string_literal: true

require 'service'
require 'om_swami/books/feed'

class SaveLatestBooks
  include Service

  def initialize
    @books = OmSwami::Books::Feed.fetch
  end

  def execute
    initial_count = Book.count
    @books.each do |book|
      begin
        next if book_exists?(book[:title], book[:published_at])
        Book.create!(book)
      rescue ActiveRecord::RecordInvalid => err
        notify_error(err.message)
      end
    end
    notify_users if Book.count > initial_count
    Rails.logger.info 'Finished populating latest books data'
  end

  private

    def notify_users
      FirebaseCloudMessaging::UserNotificationSender.execute(
        I18n.t('book.notification_message')
      )
    end

    def book_exists?(title, published_at)
      Book.exists?(title: title, published_at: published_at)
    end
end
