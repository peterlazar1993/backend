# frozen_string_literal: true

require 'service'
require 'youtube_api'
require 'google/apis/youtube_v3'

# Updates the video category to 'Non-profits & activism'
#
class UpdateVideoCategory
  include Service

  YT = Google::Apis::YoutubeV3
  BROADCAST_CATEGORY = 29

  # @param [Google::Apis::YoutubeV3::LiveBroadcast, Google::Apis::YoutubeV3::Video] video_resource
  # video_resource is expected to define id and snippet attributes
  # resource.snippet should have title and description properties
  def initialize(video_resource)
    @client = YT::YouTubeService.new
    @client.authorization = YoutubeApi.authorize_via_google_oauth_2
    @broadcast = video_resource
  end

  def execute
    @video = @client.update_video(part, video_object, {})
    # TO DO : Handle exceptions
  end

  private

    def part
      'id,snippet,contentDetails,status'
    end

    def video_object
      Google::Apis::YoutubeV3::Video.new(
        id: @broadcast.id,
        snippet: {
          title: @broadcast.snippet.title,
          description: @broadcast.snippet.title,
          category_id: BROADCAST_CATEGORY
        }
      )
    end
end
