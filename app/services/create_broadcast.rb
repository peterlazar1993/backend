# frozen_string_literal: true

require 'service'
require 'youtube_api'
require 'google/apis/youtube_v3'


class CreateBroadcast
  include Service

  YT = Google::Apis::YoutubeV3
  BROADCAST_PRIVACY = 'unlisted'

  def initialize(title, description, start_time, stream_id)
    @client = YT::YouTubeService.new
    @client.authorization = YoutubeApi.authorize_via_google_oauth_2
    @title = title
    @description = description
    @start_time = start_time
    @stream_id = stream_id
  end

  def execute
    @broadcast = @client.insert_live_broadcast(part, metadata, {})
    bind_live_stream if @broadcast && @broadcast.id
    @broadcast
  end

  private

    def part
      'id,snippet,contentDetails,status'
    end

    def metadata
      {
        content_details: {
          # Automatically start the event when you start sending data
          enable_auto_start: true
        },
        snippet: {
          title: @title,
          description: @description,
          scheduled_start_time: @start_time,
        },
        status: {
          privacy_status: BROADCAST_PRIVACY
        }
      }
    end

    def bind_live_stream
      @client.bind_live_broadcast(@broadcast.id,
                                  part,
                                  stream_id: @stream_id)
    end
end
