# frozen_string_literal: true

require 'service'
require 'om_swami/gallery/feed'

class SaveLatestGallery
  include Service

  def initialize
    @galleries = OmSwami::Gallery::Feed.fetch
  end

  def execute
    initial_count = Gallery.count
    @galleries.each do |gallery|
      begin
        Gallery.where(name: gallery[:name], date: gallery[:date])
          .first_or_create! do |g|
          g.link = gallery[:link]
          g.cover_image = gallery[:cover_image]
          g.gallery_images = gallery[:images]
        end
      rescue ActiveRecord::RecordInvalid => err
        notify_error(err.message)
      end
    end
    notify_users if Gallery.count > initial_count
    Rails.logger.info 'Finished populating latest gallery data'
  end

  private

    def notify_users
      FirebaseCloudMessaging::UserNotificationSender.execute(
        I18n.t('gallery.notification_message')
      )
    end
end
