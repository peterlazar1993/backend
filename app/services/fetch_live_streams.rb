# frozen_string_literal: true

require 'service'
require 'youtube_api'
require 'google/apis/youtube_v3'

class FetchLiveStreams
  include Service
  YT = Google::Apis::YoutubeV3

  # @param [String] id
  # The id parameter specifies comma-separated string of YouTube broadcast IDs
  # that identify the broadcasts being retrieved.
  # example 1, only one broadcast => '1sfh45tfg'
  # example 2, multiple broadcasts => '1sfh45tfg, aFghj67ui'
  def initialize
    @client = YT::YouTubeService.new
    @client.authorization = YoutubeApi.authorize_via_google_oauth_2
  end

  def execute
    @client.list_live_streams(part, mine: true)
  end

  private

    def part
      'id,snippet,cdn,status'
    end
end
