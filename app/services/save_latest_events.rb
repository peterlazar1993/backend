# frozen_string_literal: true

require 'service'
require 'om_swami/events/feed'

class SaveLatestEvents
  include Service

  def initialize
    @events = OmSwami::Events::Feed.fetch
  end

  def execute
    initial_count = Event.count
    @events.each do |event|
      begin
        next if event_exists?(event[:link], event[:scheduled_at])
        Event.create!(event)
      rescue ActiveRecord::RecordInvalid => err
        notify_error(err.message)
      end
    end
    notify_users if Event.count > initial_count
    Rails.logger.info 'Finished populating latest events data'
  end

  private

    def notify_users
      FirebaseCloudMessaging::UserNotificationSender.execute(
        I18n.t('event.notification_message')
      )
    end

    def event_exists?(link, scheduled_at)
      Event.exists?(link: link, scheduled_at: scheduled_at)
    end
end
