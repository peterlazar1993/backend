# frozen_string_literal: true

require 'service'
require 'youtube_api'
require 'google/apis/youtube_v3'


class UpdateBroadcast
  include Service

  YT = Google::Apis::YoutubeV3
  BROADCAST_PRIVACY = 'unlisted'

  def initialize(id, title, description, start_time)
    @client = YT::YouTubeService.new
    @client.authorization = YoutubeApi.authorize_via_google_oauth_2
    @id = id
    @title = title
    @description = description
    @start_time = start_time
  end

  def execute
    @client.update_live_broadcast(part, metadata, {})
  end

  private

    def part
      'id,snippet,status'
    end

    def metadata
      {
        id: @id,
        snippet: {
          title: @title,
          description: @description,
          scheduled_start_time: @start_time,
        },
        status: {
          privacy_status: BROADCAST_PRIVACY
        }
      }
    end
end
