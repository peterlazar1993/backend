# frozen_string_literal: true

require 'service'
require 'om_swami/blog/feed'

class SaveLatestArticles
  include Service

  def initialize
    @articles = OmSwami::Blog::Feed.fetch
  end

  def execute
    initial_count = Article.count
    @articles.each do |article|
      begin
        next if article_exists?(article[:title], article[:published_at])
        Article.create!(article)
      rescue ActiveRecord::RecordInvalid => err
        notify_error(err.message)
      end
    end
    notify_users if Article.count > initial_count
    Rails.logger.info 'Finished populating latest articles data'
  end

  private

    def notify_users
      FirebaseCloudMessaging::UserNotificationSender.execute(
        I18n.t('article.notification_message')
      )
    end

    def article_exists?(title, published_at)
      Article.exists?(title: title, published_at: published_at)
    end
end
