# frozen_string_literal: true

require 'service'

class FilterAdminResources
  include Service
  #  set pages which you wanna remove from admin sidebar
  HIDDEN_PAGES = ['questions', 'quick_swaminars'].freeze

  # param @admin must be of type  Administrate::Namespace.new
  # see app/views/admin/application/_naviagtion.html.erb
  def initialize(admin)
    @admin_namesapace = admin
  end

  def execute
    @admin_namesapace.resources.reject { |i| i.resource.in?(HIDDEN_PAGES) }
  end
end
