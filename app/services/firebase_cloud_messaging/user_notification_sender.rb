# frozen_string_literal: true

require 'service'

module FirebaseCloudMessaging
  class UserNotificationSender
    include Service

    FCM_TOPIC = ENV['FCM_TOPIC'] || 'newContentStaging'
    PRIORITY = 'high'
    SOUND = 'default'

    attr_reader :message, :topic

    def initialize(message)
      @message = message
    end

    def execute
      fcm_client.send_to_topic(FCM_TOPIC, options)
    end

    private

      def options
        {
          priority: PRIORITY,
          data: {
            message: message
          },
          notification: {
            body: message,
            sound: SOUND
          }
        }
      end

      def fcm_client
        @fcm_client ||= ::FCM.new(Rails.application.secrets.fcm[:server_api_key])
      end
  end
end
