# frozen_string_literal: true

require 'service'
require 'om_swami/quotes/feed'

class SaveLatestQuotes
  include Service

  def initialize
    @quote = OmSwami::Quotes::Feed.fetch
  end

  def execute
    begin
      Quote.where(
        content: @quote[:content],
        date: @quote[:date]
      ).first_or_create!
    rescue ActiveRecord::RecordInvalid => err
      notify_error(err.message)
    end
    Rails.logger.info 'Finished populating latest quotes data'
  end
end
