# frozen_string_literal: true

class ArticlesController < ApplicationController
  before_action :authenticate_device!

  def index
    articles = Article.recent
    paginate json: articles, per_page: (params[:per_page] || 10)
  end
end
