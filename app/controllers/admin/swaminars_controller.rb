module Admin
  class SwaminarsController < Admin::ApplicationController
    before_action :identify_swaminar, only: :update
    # before_action :check_params, only: [:update, :create]
    before_action :check_params_for_create, only: :create
    before_action :check_params_for_update, only: :update

    def create
      broadcast, live_stream = Admin::SwaminarFormObject.new(
        nil, swaminar_params
      ).create_broadcast_with_livestream
      @swaminar = Swaminar.new(broadcast_slug: broadcast.id,
                               stream_id: live_stream.id)
      if @swaminar.save
        redirect_to(admin_swaminar_path(@swaminar),
          notice: I18n.t('swaminar.admin_create_success'),
        )
      else
        error_redirect(I18n.t 'swaminar.admin_create_error')
      end
    end

    def update
      @swaminar.broadcast_slug = swaminar_params[:broadcast_slug]
      liveBoradcast = Admin::SwaminarFormObject.new(@swaminar, swaminar_params).live_broadcast_data
      if liveBoradcast.items.present?
        @swaminar.stream_id = liveBoradcast.items.first.content_details.try(:bound_stream_id)
      else
        error_redirect(I18n.t 'swaminar.admin_update_error')
        return
      end
      broadcast, livestream = Admin::SwaminarFormObject.new(
        @swaminar, swaminar_params
      ).update_resources
      if broadcast.id.present? && livestream.id.present? && @swaminar.save
        redirect_to(
          admin_swaminar_path(@swaminar),
          notice: I18n.t('swaminar.admin_update_success'),
        )
      else
        error_redirect(I18n.t 'swaminar.admin_update_error')
      end
    end

    def export
      Swaminar.create_csv(params[:id])
      # Question.create_csv(params[:id])
      send_file("questions.csv",filename:"questions.csv")
    end

    private

      def swaminar_params
        params.require(:swaminar).permit(
          :title, :description, :scheduled_at, :stream_title, :broadcast_slug
        )
      end

      def params_missing?
        swaminar_params[:title].empty? ||
        swaminar_params[:description].empty? ||
        swaminar_params[:scheduled_at].empty? ||
        swaminar_params[:stream_title].empty?
      end

      def identify_swaminar
        @swaminar = Swaminar.find(params[:id])
      end

      def error_redirect(msg)
        redirect_back(
          fallback_location: admin_swaminars_path,
          flash: { error: msg }
        )
      end

      def check_params
        error_redirect(I18n.t 'swaminar.admin_parameters_error') if params_missing?
      end

      def params_missing_for_create
          swaminar_params[:title].empty? ||
          swaminar_params[:description].empty? ||
          swaminar_params[:scheduled_at].empty? ||
          swaminar_params[:stream_title].empty?
      end

      def params_missing_for_update
          swaminar_params[:title].empty? ||
          swaminar_params[:description].empty? ||
          swaminar_params[:scheduled_at].empty? ||
          swaminar_params[:stream_title].empty? ||
          swaminar_params[:broadcast_slug].empty?
      end

      def check_params_for_create
        error_redirect(I18n.t 'swaminar.admin_parameters_error') if params_missing_for_create
      end

      def check_params_for_update
        error_redirect(I18n.t 'swaminar.admin_parameters_error') if params_missing_for_update
      end
  end
end
