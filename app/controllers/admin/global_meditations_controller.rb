module Admin
  class GlobalMeditationsController < Admin::ApplicationController
    before_action :default_params
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = GlobalMeditation.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   GlobalMeditation.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
    def scoped_resource
      resource_class.unscoped
    end

    def default_params
      params[:order] ||= 'scheduled_at'
      params[:direction] ||= 'desc'
    end
  end
end
