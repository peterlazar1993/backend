class Users::PasswordsController < DeviseTokenAuth::PasswordsController
    wrap_parameters false

    # this action is responsible for generating password reset tokens and
    # sending emails
    # This action is overridden to allow passwod resetd for users signed-up via
    # social media
    # this override is specific to devise_token_auth 0.1.42
    #
    # Changes
    # =======
    # variable q
    # [original gem implementation] q = "uid = ? AND provider='email'"
    # [override] q = "uid = ?"
    # [original gem implementation]
    # if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
    #    q = "BINARY uid = ? AND provider='email'"
    #  end
    # [override]
    # if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
    #    q = "BINARY uid = ?"
    #  end
    def create
      unless resource_params[:email]
        return render_create_error_missing_email
      end

      # give redirect value from params priority
      @redirect_url = params[:redirect_url]

      # fall back to default value if provided
      @redirect_url ||= DeviseTokenAuth.default_password_reset_url

      unless @redirect_url
        return render_create_error_missing_redirect_url
      end

      # if whitelist is set, validate redirect_url against whitelist
      if DeviseTokenAuth.redirect_whitelist
        unless DeviseTokenAuth::Url.whitelisted?(@redirect_url)
          return render_create_error_not_allowed_redirect_url
        end
      end

      # honor devise configuration for case_insensitive_keys
      if resource_class.case_insensitive_keys.include?(:email)
        @email = resource_params[:email].downcase
      else
        @email = resource_params[:email]
      end

      q = "uid = ?"

      # fix for mysql default case insensitivity
      if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
        q = "BINARY uid = ?"
      end

      @resource = resource_class.where(q, @email).first

      @errors = nil
      @error_status = 400

      if @resource
        yield @resource if block_given?
        @resource.send_reset_password_instructions({
          email: @email,
          provider: 'email',
          redirect_url: @redirect_url,
          client_config: params[:config_name]
        })

        if @resource.errors.empty?
          return render_create_success
        else
          @errors = @resource.errors
        end
      else
        @errors = [I18n.t("devise_token_auth.passwords.user_not_found", email: @email)]
        @error_status = 404
      end

    if @errors
      return render_create_error
    end
  end

  # This override is to disable returning error response if the provider
  # is not 'email'. This check is commented out
  def update
    # make sure user is authorized
    unless @resource
      return render_update_error_unauthorized
    end

    # make sure account doesn't use oauth2 provider
    # unless @resource.provider == 'email'
    #   return render_update_error_password_not_required
    # end

    # ensure that password params were sent
    unless password_resource_params[:password] && password_resource_params[:password_confirmation]
      return render_update_error_missing_password
    end

    if @resource.send(resource_update_method, password_resource_params)
      @resource.allow_password_change = false

      yield @resource if block_given?
      return render_update_success
    else
      return render_update_error
    end
  end
end
