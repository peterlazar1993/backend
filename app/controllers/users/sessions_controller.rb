class Users::SessionsController < DeviseTokenAuth::SessionsController
  wrap_parameters false
  require 'jwt'
  include DeviceManager

  def social_sign_in
    if social_login_params[:provider] == 'google'
      userinfo = JWT.decode(social_login_params[:id_token], nil, false).first
    else
      render_error(422, I18n.t('users.missing_provider')) and return
    end

    @user = User.find_by_email(userinfo['email'])
    raise ActiveRecord::RecordNotFound if @user.nil?

    # tokens
    @client_id = SecureRandom.urlsafe_base64(nil, false)
    @token     = SecureRandom.urlsafe_base64(nil, false)
    @user.tokens[@client_id] = {
      token: BCrypt::Password.create(@token),
      expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
    }
    auth_header = @user.build_auth_header(@token, @client_id)
    # update the response header
    response.headers.merge!(auth_header)
    # save user
    @user.save!
    # sign in user
    if sign_in(:user, @user, store: false, bypass: false)
      # register device against current user
      add_user_device(@user)
      render json:  @user.token_validation_response
    end
  rescue ActiveRecord::RecordNotFound
    render_error(422, I18n.t('users.email_not_exists', email: userinfo['email']))
  rescue ActiveRecord::RecordInvalid
    render_error(422, 'Unable to sign in', @user.errors.messages)
  rescue JWT::DecodeError
    render_error(422, I18n.t('users.invalid_jwt'))
  end

  def social_login_params
    params.permit(:provider, :id_token, :device_id)
  end

  # override to disable provider == 'email' check
  # this is to allow users signed up via google/facebook
  # to login as like email login after a password reset
  def create
    # Check
    field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

    @resource = nil
    if field
      q_value = resource_params[field]

      if resource_class.case_insensitive_keys.include?(field)
        q_value.downcase!
      end

      # this is been overridden
      # original version as per 1.42.1
      # q = "#{field.to_s} = ? AND provider='email'"
      q = "#{field.to_s} = ?"

      if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
        q = "BINARY " + q
      end

      @resource = resource_class.where(q, q_value).first
    end

    if @resource && valid_params?(field, q_value) && (!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
      valid_password = @resource.valid_password?(resource_params[:password])
      if (@resource.respond_to?(:valid_for_authentication?) && !@resource.valid_for_authentication? { valid_password }) || !valid_password
        render_create_error_bad_credentials
        return
      end
      # create client id
      @client_id = SecureRandom.urlsafe_base64(nil, false)
      @token     = SecureRandom.urlsafe_base64(nil, false)

      @resource.tokens[@client_id] = {
        token: BCrypt::Password.create(@token),
        expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
      }
      @resource.save

      sign_in(:user, @resource, store: false, bypass: false)
      # register device against current user
      add_user_device(@resource)
      yield @resource if block_given?

      render_create_success
    elsif @resource && !(!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
      render_create_error_not_confirmed
    else
      render_create_error_bad_credentials
    end
  end

  protected

    def render_create_success
      render json: resource_data(resource_json: @resource.token_validation_response)
    end

    def render_create_error_not_confirmed
      if @resource.respond_to?(:blacklisted?) && @resource.blacklisted?
        render json: {
          success: false,
          errors: [ I18n.t('users.blacklisted', email: @resource.email) ]
        }, status: 401
      else
        super
      end
    end

    def render_error(status, message, data = nil)
      response = {
        success: false,
        errors: [message]
      }
      response = response.merge(data: data) if data
      render json: response, status: status
    end
end
