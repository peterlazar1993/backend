# frozen_string_literal: true

class Users::RegistrationsController < DeviseTokenAuth::RegistrationsController
  wrap_parameters false
  require 'jwt'
  include DeviceManager

  def social_sign_up
    @resource = resource_class.new
    if sign_up_params[:provider] == 'google'
      userinfo = JWT.decode(sign_up_params[:id_token], nil, false).first
      @resource.email = userinfo['email']
      @resource.image = userinfo['picture']
      @resource.provider = 'google'
      @resource.name = sign_up_params[:name]
      @resource.country = sign_up_params[:country]
      @resource.city = sign_up_params[:city]
      @resource.gender = sign_up_params[:gender]
    else
      render_error(422, I18n.t('users.missing_provider')) and return
    end
    # assign random password
    p = SecureRandom.urlsafe_base64(nil, false)
    @resource.password = p
    @resource.password_confirmation = p
    # set uid
    @resource.uid = @resource.email
    @resource.confirmed_at = Time.now
    # tokens
    @client_id = SecureRandom.urlsafe_base64(nil, false)
    @token     = SecureRandom.urlsafe_base64(nil, false)
    @resource.tokens[@client_id] = {
      token: BCrypt::Password.create(@token),
      expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
    }
    auth_header = @resource.build_auth_header(@token, @client_id)
    # update the response header
    response.headers.merge!(auth_header)
    # save user
    @resource.save!
    # sign in user
    if sign_in(:user, @resource, store: false, bypass: false)
      # register device against current user
      add_user_device(@resource)
      render json:  @resource.token_validation_response
    end
  rescue JWT::DecodeError
    render_error(422, I18n.t('users.invalid_jwt'))
  rescue ActiveRecord::RecordNotUnique
    render_create_error_email_already_exists
  rescue ActiveRecord::RecordInvalid
    render_error(422, 'Unable to sign up', @resource.errors.messages)
  end

  protected

    def sign_up_params
      params.permit(
        :id_token,
        :email,
        :provider,
        :password,
        :name,
        :image,
        :country,
        :city,
        :gender,
        :device_id
      )
    end

    def render_create_success
      render json: resource_data(resource_json: @resource.token_validation_response)
    end

    def render_create_error_email_already_exists
      response = {
        status: 'error',
        data:   resource_data
      }
      message = I18n.t('devise_token_auth.registrations.email_already_exists', email: @resource.email, provider: @resource.provider.capitalize)
      render_error(422, message, response)
    end

    def render_error(status, message, data = nil)
      response = {
        success: false,
        errors: [message]
      }
      response = response.merge(data: data) if data
      render json: response, status: status
    end
end
