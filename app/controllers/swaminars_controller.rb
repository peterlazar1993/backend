# frozen_string_literal: true

class SwaminarsController < ApplicationController
  before_action :authenticate_device!, only: :index
  before_action :authenticate_user!,
                :find_swaminar,
                except: %i[index attendees_by_location]

  def index
    # https://github.com/rails/rails/issues/26075
    # https://github.com/rspec/rspec-rails/issues/610
    if params[:elapsed] == true || params[:elapsed] == 'true'
      swaminars = Swaminar.elapsed
    else
      swaminars = Swaminar.upcoming
    end
    render json: swaminars, per_page: (params[:per_page] || 10)
  end


  def join
    attendee = SwaminarAttendeeFormObject.new(
      current_user, @swaminar,
      request.ip, params
    ).join
    render json: { coordinates: [attendee.longitude, attendee.latitude] },
                  status: :created
  end

  def unjoin
    SwaminarAttendeeFormObject.new(current_user, @swaminar).unjoin
    render json: {}, status: :no_content
  end

  def attendees_by_location
    attendee_info = []
    attendees = SwaminarAttendee.attendees_by_location(params[:swaminar_id])
    attendees.each do | key, value |
      attendee_info << {
        name: "#{key[0]},#{key[1]}",
        coordinates: key[2..3],
        attendees_count: value
      }
    end
    render json: attendee_info
  end

  private

    def find_swaminar
      @swaminar ||= Swaminar.find(params[:id])
    end
end
