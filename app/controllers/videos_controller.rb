class VideosController < ApplicationController
  before_action :authenticate_device!
  before_action :authenticate_user!, only: [:favorite, :unfavorite]

  def index
    if params[:filter_type] == 'favorites'
      user = current_user || User.new
      videos = user.favorited_videos.of_category(params[:video_category_id]).latest_first
    else
      videos = Video.of_category(params[:video_category_id]).latest_first
    end
    paginate json: videos, per_page: params[:per_page]
  end

  def favorite
    video = Video.find params[:id]
    if Favorite.find_by(user: current_user, favorable: video).present?
      respond_with :ok
    else
      Favorite.create user: current_user, favorable: video
      respond_with :created
    end
  end

  def unfavorite
    video = Video.find params[:id]
    favorite = Favorite.find_by(user: current_user, favorable: video)
    favorite.delete if favorite.present?
    respond_with :ok
  end

  private

  def respond_with(http_status)
    render json: {}, status: http_status
  end
end
