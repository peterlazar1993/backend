# frozen_string_literal: true

class EventsController < ApplicationController
  before_action :authenticate_device!

  def index
    if request.query_parameters[:order_by] == 'DESC'
      events = Event.upcoming.order('scheduled_at DESC')
    else
      events = Event.upcoming.order('scheduled_at ASC')
    end
    paginate json: events, per_page: (params[:per_page] || 10)
  end
end
