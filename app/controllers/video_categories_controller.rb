class VideoCategoriesController < ApplicationController
  before_action :authenticate_device!

  # GET /video_categories
  def index
    video_categories = VideoCategory.all
    render json: video_categories
  end
end
