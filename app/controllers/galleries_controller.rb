# frozen_string_literal: true

class GalleriesController < ApplicationController
  before_action :authenticate_device!

  def index
    galleries = Gallery.recent
    paginate json: galleries, per_page: (params[:per_page] || 10)
  end
end
