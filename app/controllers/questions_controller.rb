# frozen_string_literal: true

class QuestionsController < ApplicationController
  before_action :authenticate_user!, :find_swaminar

  def index
    @questions = Question.where(askable: @swaminar)
    @user_questions = @questions.where(user: current_user)
    # get_up_voted is a method available form act_as_votable gem
    @upvotes = current_user.get_up_voted @questions
    render json: {
      questions: ActiveModel::Serializer::CollectionSerializer.new(
        @questions, each_serializer: QuestionSerializer
      ),
      user_questions: @user_questions.pluck(:id),
      user_upvoted: @upvotes.pluck(:id) }, status: :ok
  end

  def create
    @question = @swaminar.questions.build(question_params)
    if @question.save
      render json: {}, status: :created
    else
      render json: { errors: @question.errors.messages }, status: 422
    end
  end

  def update
    @question = @swaminar.questions.find(params[:id])
    authorize @question
    if @question.update(question_params)
      render json: {}, status: :ok
    else
      render json: { errors: @question.errors.messages }, status: 422
    end
  end

  def upvote
    @question = @swaminar.questions.find(params[:id])
    @question.liked_by current_user
    render json: { upvotes: @question.get_likes.size }, status: :created
  end

  def undo_vote
    @question = @swaminar.questions.find(params[:id])
    @question.unliked_by current_user
    render json: { upvotes: @question.get_likes.size }, status: :ok
  end

  private

    def find_swaminar
      @swaminar ||= Swaminar.find(params[:swaminar_id])
    end

    def question_params
      params.require(:question).permit(:body).merge(user_id: current_user.id)
    end
end
