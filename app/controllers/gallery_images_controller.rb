# frozen_string_literal: true

class GalleryImagesController < ApplicationController
  before_action :authenticate_device!

  def index
    images = GalleryImage.where(gallery: gallery)
    paginate json: images, per_page: (params[:per_page] || 10)
  end

  private

    def gallery
      Gallery.find_by(id: params[:id])
    end
end
