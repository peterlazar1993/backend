# frozen_string_literal: true

module DeviceManager
  extend ActiveSupport::Concern

  included do
    after_action :remove_user_device, only: :destroy
  end

  private

    def add_user_device(user)
      device.update(user: user) if device && user
    end

    def remove_user_device
      device.update(user: nil) if device
    end

    def device
      @device ||= Device.find_by(uid: params[:device_id])
    end
end
