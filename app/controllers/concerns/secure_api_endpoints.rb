# frozen_String_literal: true

module SecureApiEndpoints
  extend ActiveSupport::Concern
  include ActionController::HttpAuthentication::Token::ControllerMethods

  protected

    def authenticate_device!
      authenticate_or_request_with_http_token do |token, options|
        Device.exists?(uid: token)
      end
    end
end
