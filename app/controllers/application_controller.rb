# frozen_string_literal: true

require 'error/error_handler'

class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionController::MimeResponds
  include Error::ErrorHandler
  include Pundit
  include SecureApiEndpoints

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_in, keys: [:device_id])
      devise_parameter_sanitizer.permit(:sign_up, keys: [:device_id])
    end
end
