class CategoriesController < ApplicationController
  before_action :authenticate_device!

  def index
    categories = Category.all.to_a
    categories.insert(0,Category.all_category)
    render json: categories
  end
end
