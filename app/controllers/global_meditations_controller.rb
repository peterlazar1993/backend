class GlobalMeditationsController < ApplicationController
  before_action :authenticate_device!
  before_action :authenticate_user!, only: [:join, :unjoin]

  def index
    # https://github.com/rails/rails/issues/26075
    # https://github.com/rspec/rspec-rails/issues/610
    if params[:elapsed] == true || params[:elapsed] == 'true'
      meditations = GlobalMeditation.elapsed
                                    .exclude_active_at(Time.current)
    else
      meditations = GlobalMeditation.upcoming
    end
    paginate json: meditations, per_page: (params[:per_page] || 10)
  end


  def join

    participant = GlobalMeditationParticipant.find_or_create_by(
      user: current_user,
      global_meditation: GlobalMeditation.find(params[:id]),
    )
    participant.update_attributes(
      ip_address: request.ip,
      latitude: params[:latitude],
      longitude: params[:longitude]
    )
    render json: {coordinates: [participant.longitude,participant.latitude]}, status: :created
  end

  def unjoin
    GlobalMeditationParticipant.where(
      user: current_user,
      global_meditation: GlobalMeditation.find(params[:id])).destroy_all
    render json: {}, status: :no_content
  end

  def attendees_by_location
    meditator_info = []
    participants = GlobalMeditationParticipant.joined_meditators_by_location(params[:id])
    participants.each do |key, value|
      meditator_info << {
        name: "#{key[0]},#{key[1]}",
        coordinates: key[2..3],
        attendees_count: value
      }
    end
    render json: meditator_info
  end
end
