class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:stats]
  def stats
    render json: current_user
  end
end
