# frozen_string_literal: true

class QuotesController < ApplicationController
  before_action :authenticate_device!

  def index
    quotes = Quote.recent
    paginate json: quotes, per_page: (params[:per_page] || 10)
  end
end
