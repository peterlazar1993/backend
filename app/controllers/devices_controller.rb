class DevicesController < ApplicationController
  def create
    device = Device.find_or_create_by(device_params)
    render json: { token: device.uid }, status: :created
  end

  private
  def device_params
    params.require(:device).permit(
      :device_id,
      :platform,
      :push_token
    )
  end

end
