class Meditations::SessionsController < ApplicationController
  before_action :authenticate_user!

  def create
    @session = ::SessionFormObject.new(current_user, params).new_session
    if @session.save
      render json: { session: { id: @session.id } }, status: :created
    else
      render json: @session.errors, status: :unprocessable_entity
    end
  end
end
