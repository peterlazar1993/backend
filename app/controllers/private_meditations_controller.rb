class PrivateMeditationsController < ApplicationController
  before_action :authenticate_device!
  before_action :authenticate_user!, only: [:favorite, :unfavorite]

  def index
    render json: PrivateMeditation.order('created_at DESC')
  end

  def favorite
    Favorite.find_or_create_by(
      user: current_user,
      favorable: PrivateMeditation.find(params[:id])
    )
    render json: {}, status: :created
  end

  def unfavorite
    Favorite.where(
        user: current_user,
        favorable: PrivateMeditation.find(params[:id])
      ).destroy_all

    render json: {}, status: :no_content
  end
end
