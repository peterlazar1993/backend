# frozen_string_literal: true

require 'service'
require 'youtube_api/client'

class PopulateYoutubeDataJob
  include Service

  def execute
    youtube_client = YoutubeApi::Client.new(
      api_key: Rails.application.secrets.youtube_api_key
    )
    VideoCategory.all.each do |category|
      page_token = ''
      loop do
        response = youtube_client.get_videos(
          channel_id: category.youtube_channel_id,
          page_token: page_token
        )
        video_hashes = response.video_hashes.map do |video_hash|
          video_hash[:video_category_id] = category.id
          video_hash
        end
        Video.create video_hashes
        break if response.last_page?
        page_token = response.next_page_token
      end
    end
    Rails.logger.info "Finished populating Youtube video data"
  end
end
