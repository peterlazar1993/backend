# frozen_string_literal: true

class SwaminarAttendeeFormObject
  include ActiveModel::Model

  attr_accessor :ip, :latitude, :longitude

  def self.model_name
    ActiveModel::Name.new(self, nil, 'SwaminarAttendee')
  end

  def initialize(current_user, swaminar, ip = nil, params = {})
    @current_user = current_user
    @swaminar = swaminar
    self.ip = ip
    self.latitude = params[:latitude]
    self.longitude = params[:longitude]
  end

  def join
    attendee = SwaminarAttendee.find_or_create_by(
      user: @current_user,
      swaminar: @swaminar,
    )
    attendee.update(ip_address: ip,
      latitude: latitude,
      longitude: longitude
    )
    attendee
  end

  def unjoin
    @swaminar.attendees.where(user: @current_user).destroy_all
    @swaminar.questions.where(user: @current_user).destroy_all
  end
end
