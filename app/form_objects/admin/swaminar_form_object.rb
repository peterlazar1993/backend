# frozen_string_literal: true

class Admin::SwaminarFormObject
  include ActiveModel::Model

  attr_accessor :title, :description, :scheduled_time, :stream_title

  def self.model_name
    ActiveModel::Name.new(self, nil, 'Swaminar')
  end

  def initialize(swaminar = nil, params = {})
    @swaminar = swaminar
    @title = params[:title]
    @description = params[:description]
    @scheduled_time = params[:scheduled_at]
    @stream_title = params[:stream_title]
  end

  def create_broadcast_with_livestream
    live_stream = CreateLiveStream.execute(stream_title)
    broadcast = CreateBroadcast.execute(
      title,
      description,
      scheduled_time.in_time_zone('Kolkata').iso8601,
      live_stream.id
    )
    # update category to Non-profit & activism
    UpdateVideoCategory.execute(broadcast)
    # expilcitly return both broadcast and livestream
    [broadcast, live_stream]
  end

  def update_resources
    broadcast = UpdateBroadcast.execute(
      @swaminar.broadcast_slug,
      title,
      description,
      scheduled_time.to_time.iso8601
    )
    livestream = UpdateLiveStream.execute(@swaminar.stream_id, stream_title)
    [broadcast, livestream]
    # TO DO : Handle exceptions
  end

  def live_broadcast_data
    broadcast = FetchBroadcastData.execute(@swaminar.broadcast_slug)
  end
end
