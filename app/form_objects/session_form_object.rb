# frozen_string_literal: true

class SessionFormObject
  include ActiveModel::Model

  attr_accessor :user, :params

  def self.model_name
    ActiveModel::Name.new(self, nil, 'Session')
  end

  def initialize(user, params)
    @user = user
    @params = params
  end

  def new_session
    @session = Session.new(
      duration_in_minutes: session_param[:duration_in_minutes],
      meditation_type: session_param[:meditation_type],
      meditation_type_id: session_param[:meditation_type_id],
      played_at: session_param[:played_at],
      user: user
    )
    @session.build_feedback(notes: feedback_param[:notes],
                            ratings: feedback_param[:ratings])
    @session # implicit return is mandatory
  end

  private

    def session_param
      @params[:session] || {}
    end

    def feedback_param
      @params[:feedback] || {}
    end
end
