# frozen_string_literal: true

class GalleryImage < ApplicationRecord
  belongs_to :gallery
end
