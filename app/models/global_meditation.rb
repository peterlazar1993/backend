class GlobalMeditation < ApplicationRecord
  include Notifiable
  include AudioDurationCalculator

  default_scope { order(scheduled_at: :asc) }
  scope :upcoming, -> { where('scheduled_at >= ?', 5.minutes.ago) }
  scope :elapsed, -> { where('scheduled_at < ?', Time.current) }
  scope :exclude_active_at, ->(current_time) do
    select { |obj| current_time > (obj.scheduled_at + obj.duration_in_minutes.minutes) }
  end

  has_many :meditators, class_name: 'GlobalMeditationParticipant', dependent: :destroy

  validates :scheduled_at, :track, presence: true

  has_attached_file :track

  validates_attachment_content_type :track,
    :content_type => ['audio/mpeg','audio/mp3']

  def update_duration_for_tracks
    self.duration_in_minutes = extract_duration_from_mp3_info(
      track,self.duration_in_minutes)
  end
end
