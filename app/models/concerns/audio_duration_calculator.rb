module AudioDurationCalculator
  extend ActiveSupport::Concern
  included do
    before_save :update_duration_for_tracks
  end

  def extract_duration_from_mp3_info track,current_field_value
    duration = current_field_value
    if track.content_type && track.queued_for_write[:original]
      path = track.queued_for_write[:original].path
      open_opts = { :encoding => 'utf-8' }
      Mp3Info.open(path, open_opts) do |mp3info|
        duration = (mp3info.length / 60)
      end
    end
    return duration
  end
end
