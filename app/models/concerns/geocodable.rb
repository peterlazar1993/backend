# frozen_string_literal: true

module Geocodable
  extend ActiveSupport::Concern

  included do
    before_update :reverse_geocode, :geocode

    geocoded_by :address

    reverse_geocoded_by :latitude, :longitude do |obj, results|
      if geo = results.first
        obj.location = {
          city: geo.city,
          state: geo.state,
          country: geo.country_code,
          latitude: obj.latitude,
          longitude: obj.longitude
        }
      end
    end
  end

  def address
    return unless location
    location.values_at('city', 'state', 'country').compact.join(', ')
  end
end
