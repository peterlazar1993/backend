module Favoritable
  extend ActiveSupport::Concern

  included do
    has_many :favorites, as: :favorable
  end

  def favorites_count
    favorites.count
  end
end
