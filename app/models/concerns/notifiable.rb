module Notifiable
  extend ActiveSupport::Concern

  included do
    after_create :notify_users
  end

  def notify_users
    message = I18n.t("#{self.class.model_name.singular}.notification_message")
    FirebaseCloudMessaging::UserNotificationSender.execute(message)
    logger.info 'Notified Users'
  end
end
