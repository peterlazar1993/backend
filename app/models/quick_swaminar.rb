# frozen_string_literal: true

class QuickSwaminar < ApplicationRecord
  self.table_name = 'swaminars'
  validates :broadcast_slug, presence: true
end
