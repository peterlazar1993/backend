class Category < ApplicationRecord
  ALL_CATEGORY_NAME = "All"
  ALL_CATEGORY_ID = -1

  has_many :private_meditations, dependent: :destroy

  def self.all_category
    Category.new(
      id: ALL_CATEGORY_ID,
      name: ALL_CATEGORY_NAME,
      count_of_meditations: PrivateMeditation.count,
      created_at: Time.now,
      updated_at: Time.now
    )
  end
end
