# frozen_string_literal: true

class Quote < ApplicationRecord
  validates :content, :date, presence: true
  validates :content, uniqueness: { scope: :date }

  scope :recent, -> { order('date DESC') }
end
