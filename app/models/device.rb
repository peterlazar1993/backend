class Device < ApplicationRecord
  ANDROID = "android"
  IOS = "ios"
  belongs_to :user

  validates :device_id,:platform,:push_token, presence: true
  before_create :generate_uid

  scope :with_push_token, -> {
    where.not(push_token: nil)
  }

  scope :android, -> {
    with_push_token.where(platform: ANDROID)
  }

  scope :ios, -> {
    with_push_token.where(platform: IOS)
  }

  def generate_uid
    self.uid = SecureRandom.uuid
  end
end
