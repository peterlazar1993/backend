# frozen_string_literal: true

class Swaminar < ApplicationRecord
  include Notifiable

  validates :broadcast_slug, presence: true
  validates :stream_id, presence: true
  default_scope { order(created_at: :asc) }
  has_many :attendees, class_name: 'SwaminarAttendee', dependent: :destroy
  has_many :questions, as: :askable, dependent: :destroy
  scope :elapsed, -> { select { |sw| sw.scheduled_at < 30.minutes.ago } }
  scope :upcoming, -> { select { |sw| sw.scheduled_at >= 30.minutes.ago } }

  YOUTUBE_BASE = 'https://www.youtube.com/watch?v='

  def broadcast_data
    @data ||= FetchBroadcastData.execute(broadcast_slug) if broadcast_slug
  end

  def livestream
    @stream ||= FetchLiveStream.execute(stream_id) if stream_id
  end

  def title
    broadcast_item.try(:title)
  end

  def description
    broadcast_item.try(:description)
  end

  def scheduled_at
    time_string = broadcast_item.try(:scheduled_start_time)
    time_string.in_time_zone('Kolkata') if time_string
  end

  def broadcast_link
    YOUTUBE_BASE + broadcast_slug if broadcast_slug
  end

  def stream_name
    livestream_item.try(:cdn).try(:ingestion_info).try(:stream_name)
  end

  def stream_title
    livestream_item.try(:snippet).try(:title)
  end

  def self.create_csv(id)
    CSV.open("questions.csv", "wb") do |csv|
      csv << ["Name","Question","Votes","Date"]
      Swaminar.find(id).questions.sort_by {|question| -question.votes_count}.each do |question|
        csv << [question.asked_by,question.body,question.votes_count,question.created_at]
      end
    end
  end

  private

    def broadcast_item
      return unless broadcast_data && broadcast_data.items.present?
      broadcast_data.items.first.snippet
    end

    def livestream_item
      return unless livestream && livestream.items.present?
      livestream.items.first
    end
end
