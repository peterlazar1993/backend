# frozen_string_literal: true

class Article < ApplicationRecord
  validates :title,
            :published_at,
            :summary,
            :cover_image,
            :link, presence: true

  validates :title, uniqueness: { scope: :published_at }

  scope :recent, -> { order('published_at DESC') }
end
