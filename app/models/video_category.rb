class VideoCategory < ApplicationRecord
  validates :youtube_channel_id, uniqueness: true
end
