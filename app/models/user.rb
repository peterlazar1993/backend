class User < ApplicationRecord
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable
  include DeviseTokenAuth::Concerns::User
  # enum field for gender
  enum gender: [:undisclosed, :male, :female, :other]
  # associations
  has_many :devices
  has_many :sessions
  has_many :favorites
  has_many :favorited_private_meditations, through: :favorites,
    source: :favorable, source_type: PrivateMeditation.to_s
  has_many :favorited_videos, through: :favorites,
    source: :favorable, source_type: Video.to_s
  has_many :questions
  # validations
  # validates :country, presence: true
  # user can upvote and downvote certain models
  acts_as_voter

  def total_days_meditated
    sessions.group_by { |c| c.played_at.to_date }.keys.count || 0
  end

  def consistency
    oldest_session = sessions.order(:played_at).limit(1)
    starting_date = oldest_session.empty? ? Date.today : oldest_session.first.played_at
    total_days_meditated / ((starting_date.to_date..Date.today).count).fdiv(7)
  end

  # Overriding `active_for_authentication?` implemented by devise_token_auth
  # this overide is to implement user blacklist feature
  def active_for_authentication?
    super && !blacklisted?
  end
end
