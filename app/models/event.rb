# frozen_string_literal: true

class Event < ApplicationRecord
  validates :title,
            :scheduled_at,
            :description,
            :link, presence: true

  validates :title, uniqueness: { scope: :scheduled_at }

  scope :upcoming, -> { where('scheduled_at >= ?', Date.today) }
end
