class SessionFeedback < ApplicationRecord
  self.table_name = "session_feedback"
  belongs_to :session

  validates :session, :ratings, presence: true

  store_accessor  :ratings
end
