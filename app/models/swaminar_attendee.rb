class SwaminarAttendee < ApplicationRecord
  include Geocodable

  belongs_to :swaminar, required: true, counter_cache: :attendees_count
  belongs_to :user, required: true

  scope :attendees_by_location, ->(swaminar_id) {
    where(swaminar_id: swaminar_id)
    .group(%q{location, location -> 'city'},
           %q{location, location -> 'country'}, :longitude, :latitude)
    .count
  }
end
