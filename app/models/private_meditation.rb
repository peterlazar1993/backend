class PrivateMeditation < ApplicationRecord
  include Notifiable
  include AudioDurationCalculator

  belongs_to :category, counter_cache: :count_of_meditations

  has_attached_file :guided_track
  has_attached_file :music_track
  has_many :favorites, as: :favorable, dependent: :destroy

  validates_attachment_content_type :guided_track,
    :content_type => ['audio/mpeg','audio/mp3']
  validates_attachment_content_type :music_track,
    :content_type => ['audio/mpeg','audio/mp3']

    def favorited_by_user?(user)
      favorites.find_by(user_id: user.id).present?
    end

  # Retrieves metadata for MP3s
  def update_duration_for_tracks
    self.guided_track_duration_in_minutes = extract_duration_from_mp3_info(
      guided_track,self.guided_track_duration_in_minutes)
    self.music_track_duration_in_minutes = extract_duration_from_mp3_info(
      music_track,self.music_track_duration_in_minutes)
    update_duration
  end

  def update_duration
    self.duration_in_minutes = self.guided_track_duration_in_minutes ||
      self.music_track_duration_in_minutes
  end
end
