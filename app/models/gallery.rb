# frozen_string_literal: true

class Gallery < ApplicationRecord
  validates :name, :date, :link, :cover_image, presence: true
  validates :name, uniqueness: { scope: :date }

  has_many :gallery_images, dependent: :destroy

  scope :recent, -> { includes(:gallery_images).order('date DESC') }
end
