class Session < ApplicationRecord
  LONGEST_STREAK_SQL = <<-SQL
    WITH
      groups(date, grp) AS (
        SELECT DISTINCT
          CAST(played_at AS DATE),
          CAST(played_at AS DATE) - (DENSE_RANK() OVER (ORDER BY CAST(played_at AS DATE))::integer)
          AS grp
        FROM sessions
        WHERE user_id = :user_id
        AND played_at::date <= ((TIMESTAMP 'today')::date)
      )
    SELECT
      COUNT(*) AS longest_streak_in_days,
      MIN(date) AS longest_streak_start_date,
      MAX(date) AS longest_streak_end_date
    FROM groups
    GROUP BY grp
    ORDER BY longest_streak_in_days DESC
    limit 1
  SQL


  # https://stackoverflow.com/questions/27533867/calculate-consecutive-days-posting-in-rails
  CURRENT_STREAK_SQL = <<-SQL
    SELECT (CURRENT_DATE - series_date::date) AS current_streak,
           (series_date::date + 1) AS current_streak_start_date,
           CURRENT_DATE AS current_streak_end_date
      FROM generate_series(
             ( SELECT (min(played_at::date) - 1) FROM sessions
               WHERE sessions.user_id = :user_id
             ),
             CURRENT_DATE,
             '1 day'
           ) AS series_date
    LEFT OUTER JOIN sessions ON sessions.user_id = :user_id AND
                               sessions.played_at::date = series_date
    GROUP BY series_date
    HAVING COUNT(sessions.id) = 0
    ORDER BY series_date DESC
    LIMIT 1
  SQL

  STATS_SQL = <<-SQL
    count(id) count_of_meditations,
    sum(duration_in_minutes)::integer total_meditation_in_minutes,
    avg(duration_in_minutes)::integer average_meditation_in_minutes,
    max(duration_in_minutes)::integer longest_session_in_minutes
  SQL

  belongs_to :user
  has_one :feedback, class_name: 'SessionFeedback'
  enum meditation_type: [:global, :guided, :music, :timed]
  validates :played_at, :duration_in_minutes, :meditation_type, presence: true

  scope :stats, ->(user) {
    where(user: user).where("played_at::date <= ?", Date.today).select(STATS_SQL)[0]
  }

  def self.current_streak_in_days user
    Session.find_by_sql([CURRENT_STREAK_SQL,{user_id: user}])[0]
  end

  def self.longest_streak_in_days user
    Session.find_by_sql([LONGEST_STREAK_SQL,{user_id: user}])[0]
  end
end
