# frozen_string_literal: true

require 'obscenity/active_model'

class Question < ApplicationRecord
  acts_as_votable
  validates :body,
            obscenity: { message: I18n.t('question.profanity_error') },
            presence: { message: I18n.t('question.presence_error') }
  belongs_to :user, required: true
  belongs_to :askable, counter_cache: true, polymorphic: true

  def votes_count
    votes_for.size
  end

  def asked_by
    user.name
  end
end
