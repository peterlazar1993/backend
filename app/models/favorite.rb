class Favorite < ApplicationRecord
  belongs_to :user
  belongs_to :favorable, polymorphic: true

  validates :user, :favorable, presence: true
end
