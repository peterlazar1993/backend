class Video < ApplicationRecord
  include Favoritable

  belongs_to :video_category

  validates :youtube_video_id, uniqueness: true
  validates :title, :description, :youtube_video_id,
            :thumbnail_url, :published_at,
            :video_category, presence: true

  scope :of_category, -> (category_id) {
    where(video_category_id: category_id) if category_id.present?
  }

  scope :latest_first, -> { order(published_at: :desc) }

  def self.latest
    order(published_at: :desc).first
  end

  # OPTIMIZE
  # Current implementation for favorited_by_user? is not the ideal way
  # to compute it when doing it for multiple videos. Look at the comments 
  # at the end of this file for a pure(ish) SQL solution which can perform 
  # better in such cases
  def favorited_by_user?(user)
    favorites.find_by(user_id: user.id).present?
  end
end

# SQL join query to get favorited videos of a user:
# SELECT videos.* FROM videos INNER JOIN favorites ON videos.id=favorites.favorable_id and favorites.favorable_type='Video';

# SQL join query to get videos with favorited status:
# SELECT videos.*, favorites.user_id FROM videos LEFT JOIN favorites ON videos.id=favorites.favorable_id and favorites.favorable_type='Video';
# SELECT videos.*, is_favorited(favorites.user_id) FROM videos LEFT JOIN favorites ON videos.id=favorites.favorable_id and favorites.favorable_type='Video';

# PLPGSQL function to get videos and their favorited status wrt the given user:
# CREATE FUNCTION is_favorited(favorited_user_id integer) RETURNS bool AS $$
# BEGIN
# IF favorited_user_id IS NULL
# THEN
#     RETURN false;
# ELSE
#     RETURN true;
# END IF;
# END; $$
# LANGUAGE PLPGSQL;