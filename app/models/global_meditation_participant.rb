class GlobalMeditationParticipant < ApplicationRecord
  belongs_to :global_meditation, required: true, counter_cache: :joined_meditator_count
  belongs_to :user, required: true

  geocoded_by :address

  reverse_geocoded_by :latitude, :longitude do |obj,results|
    if geo = results.first
      obj.location = {
        city: geo.city,
        state: geo.state,
        country: geo.country_code,
        latitude: obj.latitude,
        longitude: obj.longitude
      }
    end
  end
  after_validation :reverse_geocode, :geocode
  # after_create :notify_other_meditators

  scope :joined_meditators_by_location, ->(global_meditation_id) {
    where(global_meditation_id: global_meditation_id).
    group(%q{location, location -> 'city'},%q{location, location -> 'country'},:longitude,:latitude).
    count
  }

  def address
    [location["city"], location["state"], location["country"]].compact.join(', ') if location
  end

  private

  def notify_other_meditators
    ActionCable.server.broadcast(
      "global_meditations-#{self.global_meditation.id}",
      {
        "coordinates": [self.longitude, self.latitude]
      }
    )
  end

end
