# Blacklotus-admin

## Getting Started

After you have cloned this repo, run this setup script to set up your machine
with the necessary dependencies to run and test this app:

    % ./bin/setup

It assumes you have a machine equipped with Ruby, Postgres, etc. If not, set up
your machine with [this script].

[this script]: https://github.com/thoughtbot/laptop

After setting up, you can run the application by:

    % ./bin/rails s

## Running tests

Using Rspec

```
% ./bin/rspec
```

Use [Guard](https://github.com/guard/guard) to watch for code changes and run tests continuously.
```
% ./bin/guard
```

Also, it's a good idea to run the tests in a pre commit hook. That way you never push changes that have failing tests. 

This is already done for you if you have run `./bin/setup`.

If no, to set it up, add the following content in the file `.git/hooks/pre-commit`.

```
#!/bin/sh
./bin/rspec
```

This will run the tests every time you `git commit`

## Guidelines

Use the following guides for getting things done, programming well, and
programming in style.

* [Protocol](http://github.com/thoughtbot/guides/blob/master/protocol)
* [Best Practices](http://github.com/thoughtbot/guides/blob/master/best-practices)
* [Style](http://github.com/thoughtbot/guides/blob/master/style)

## Deploying

**We no longer use Heroku. Updated instructions for AWS Elastic Beanstalk should be here.**
