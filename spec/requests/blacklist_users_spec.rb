require 'rails_helper'

RSpec.describe 'Password Resets', type: :request do
  context 'blacklisted users' do
    before do
      @user = create(:user)
      @user.name = 'John Doe'
      @user.confirmed_at = Time.now
      @user.blacklisted = true
      @user.save!
    end

    it 'should not be able to login' do
      post '/auth/sign_in', params: { email: @user.email, password: @user.password }
      expect(response).to have_http_status(:unauthorized)
      error_msg = JSON.parse(response.body)['errors']
      expect(error_msg).to eq [I18n.t('users.blacklisted', email: @user.email)]
    end
  end
end
