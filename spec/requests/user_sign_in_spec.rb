require 'rails_helper'
require 'jwt'

RSpec.describe 'User login', type: :request do
  let(:provider) { 'google' }
  let(:email) { 'test@example.com' }
  let(:password) { 'mypassword@123' }
  let(:image) { 'https://example.com/photo.jpg' }
  let(:name) { 'John Doe' }
  let(:country) { 'USA' }
  let(:payload) do
    { 'email' => email,
      'email_verified' => true,
      'picture' => image
    }
  end
  let(:token) { JWT.encode(payload, nil, 'none') }

  before do
    @user = create(:user, email: email, provider: provider, uid: email,
                          password: password, password_confirmation: password,
                          image: image, confirmed_at: Time.now, name: name,
                          country: country)
  end

  describe 'Social login' do
    context 'without a valid provider' do
      it 'should not allow login' do
        post '/auth/social/sign_in', params: { id_token: token }
        expect(response).to have_http_status(:unprocessable_entity)
        json_response = JSON.parse(response.body)
        expect(json_response['errors']).to eq [I18n.t('users.missing_provider')]
      end
    end

    context 'without a valid id_token' do
      it 'should not allow login' do
        post '/auth/social/sign_in', params: { provider: provider }
        expect(response).to have_http_status(:unprocessable_entity)
        json_response = JSON.parse(response.body)
        expect(json_response['errors']).to eq [I18n.t('users.invalid_jwt')]
      end
    end

    context 'with valid params' do
      it 'should allow sign in' do
        post '/auth/social/sign_in', params: { provider: provider, id_token: token }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status(:success)
        expect(json_response['provider']).to eq provider
        expect(json_response['uid']).to eq email
        expect(json_response['image']).to eq image
        expect(json_response['email']).to eq email
        expect(json_response['name']).to eq name
        expect(json_response['country']).to eq country
      end
    end
  end

  describe 'Device management' do
    before do
      @device = create(:device)
    end
    context 'Social login' do
      it 'should register user against device on sign in' do
        post '/auth/social/sign_in', params: { provider: provider,
                                               id_token: token,
                                               device_id: @device.uid }
        expect(response).to have_http_status(:success)
        expect(@device.reload.user).to eq @user
      end
    end

    context 'Email login' do
      let(:email) { 'my@email.com' }

      it 'should register device against user on login' do
        post '/auth/sign_in', params: { email: email, password: password,
                                        device_id: @device.uid }
        expect(response).to have_http_status(:success)
        expect(@device.reload.user).to eq @user
      end
    end
  end
end
