require 'rails_helper'
require 'jwt'

RSpec.describe 'Password Resets', type: :request do
  # context 'no country specified' do
  #   let(:params) do
  #     { email: 'test@example.com',
  #       password: 'abcdef12233fff'
  #     }
  #   end
  #   it 'should not allow signup' do
  #     post '/auth', params: params
  #     expect(response).to have_http_status(:unprocessable_entity)
  #     body = JSON.parse(response.body)
  #     expect(body['errors']['full_messages']).to eq ["Country can't be blank"]
  #   end
  # end

  context 'when no gender specified' do
    let(:params) do
      { email: 'test@example.com',
        password: 'abcdef12233fff',
        country: 'India',
        name: 'John Doe'
      }
    end
    it "gender should be 'undisclosed'" do
      post '/auth', params: params
      expect(response).to have_http_status(:ok)
      body = JSON.parse(response.body)
      expect(body['gender']).to eq 'undisclosed'
    end
  end

  describe 'Social Sign Up' do
    let(:provider) { 'google' }
    let(:email) { 'test@example.com' }
    let(:image) { 'https://example.com/photo.jpg' }
    let(:name) { 'John Doe' }
    let(:country) { 'USA' }
    let(:payload) do
      { 'email' => email,
        'email_verified' => true,
        'picture' => image
      }
    end
    let(:token) { JWT.encode(payload, nil, 'none') }

    context 'without a valid provider' do
      it 'should not allow sign up' do
        post '/auth/social/sign_up', params: { name: 'Name', country: 'USA' }
        expect(response).to have_http_status(:unprocessable_entity)
        json_response = JSON.parse(response.body)
        expect(json_response['errors']).to eq [I18n.t('users.missing_provider')]
      end
    end

    context 'without a valid id_token' do
      it 'should not allow sign up' do
        post '/auth/social/sign_up', params: { provider: 'google',
                                               name: 'Name',
                                               country: 'USA' }
        expect(response).to have_http_status(:unprocessable_entity)
        json_response = JSON.parse(response.body)
        expect(json_response['errors']).to eq [I18n.t('users.invalid_jwt')]
      end
    end

    context 'with valid provider and id_token' do
      it 'should allow sign up' do
        post '/auth/social/sign_up', params: { provider: provider,
                                               name: name,
                                               country: country,
                                               id_token: token }
        expect(response).to have_http_status(:success)
        json_response = JSON.parse(response.body)
        expect(json_response['provider']).to eq provider
        expect(json_response['uid']).to eq email
        expect(json_response['image']).to eq image
        expect(json_response['email']).to eq email
        expect(json_response['name']).to eq name
        expect(json_response['country']).to eq country

        # should not allow mutiple signup for same email/id_token
        post '/auth/social/sign_up', params: { provider: provider,
                                               name: name,
                                               country: country,
                                               id_token: token }
        expect(response).to have_http_status(:unprocessable_entity)
        json_response = JSON.parse(response.body)
        expect(json_response['data']['email']).to eq ['has already been taken']
      end
    end

    context 'with valid device_id' do
      before do
        @device = create(:device)
      end

      it 'should register user against device' do
        post '/auth/social/sign_up', params: { provider: provider,
                                               name: name,
                                               country: country,
                                               id_token: token,
                                               device_id: @device.uid }
        expect(response).to have_http_status(:success)
        expect(@device.reload.user).not_to be_nil
      end
    end
  end
end
