require 'rails_helper'

RSpec.describe 'Password Resets', type: :request do
  context 'social registrations' do
    before do
      @user = create(:user)
      @user.name = 'John Doe'
      @user.provider = 'google'
      @user.confirmed_at = Time.now
      @user.save!
      @new_password = 'abcdefgh@123!'
    end

    it 'can request password reset' do
      post '/auth/password', params: { email: @user.email, redirect_url: 'avc' }
      expect(response).to be_success
    end

    it 'can reset password' do
      auth_headers = @user.create_new_auth_token
      put '/auth/password',
        params: { password: @new_password, password_confirmation: @new_password },
        headers: auth_headers
      expect(response).to be_success

      # should be able to login after password reset
      post '/auth/sign_in', params: { email: @user.email,
                                      password: @new_password }
      expect(response).to be_success
    end
  end
end
