# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Global meditation attendees by location', type: :request do

  include_context 'stub geocode api'

  before do
    @latitude = 40.7143528
    @longitude = -74.0059731
    @city = 'New York'
    @country = 'US'

    Geocoder.configure(lookup: :test)

    Geocoder::Lookup::Test.set_default_stub(
      [
        {
          'coordinates'  => [@latitude, @longitude],
          'address'      => 'New York, NY, USA',
          'state'        => 'New York',
          'city'         => @city,
          'state_code'   => 'NY',
          'country'      => 'United States',
          'country_code' => @country
        }
      ]
    )

    @attendee = create(:global_meditation_participant,
                       latitude: @latitude,
                       longitude: @longitude)

    @global_meditation = @attendee.global_meditation
    http_login
  end

  it 'should return attendees by location' do
    get "/global_meditations/#{@global_meditation.id}/attendees_by_location",
        headers: @env
    expect(response).to be_success
    response_obj = JSON.parse(response.body)
    expect(response_obj.size).to eq 1
    expect(response_obj.first['attendees_count']).to eq 1
    expect(response_obj.first['coordinates']).to eq [@attendee.longitude, @attendee.latitude]
    expect(response_obj.first['name']).to eq @city + ',' + @country
  end
end
