shared_context 'secure end-point test' do |path|
  context 'without device identification token' do
    it 'should not allow api end-point access' do
      get path
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
