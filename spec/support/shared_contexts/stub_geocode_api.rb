shared_context 'stub geocode api' do
  let(:map_data) { File.read('spec/data/location_data.json') }

  before do
    stub_request(:get, 'http://maps.googleapis.com/maps/api/geocode/json').
    with(
      query: hash_including({}),
      headers: { 'Accept' => '*/*',
      'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'User-Agent' => 'Ruby' }
    ).to_return(status: 200, body: map_data, headers: {})
  end
end
