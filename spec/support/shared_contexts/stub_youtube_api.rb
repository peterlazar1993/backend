shared_context 'stub youtube api' do
  let(:title) { 'Sample title' }
  let(:description) { 'Sample Description' }
  let(:start_time) { Time.now }


  let(:snippet) do
    double(
      'snippet',
      title: title,
      description: description,
      scheduled_start_time: start_time
    )
  end
  let(:items) do
    double('item', first: double('first', snippet: snippet))
  end

  let(:youtube_data) { double('Youtube', items: items) }

  before do
    allow_any_instance_of(YoutubeApi::OAuthClient).to receive(:authorize).
      and_return true
    allow_any_instance_of(Google::Apis::YoutubeV3::YouTubeService).
      to receive(:list_live_broadcasts).
      and_return(youtube_data)
  end
end
