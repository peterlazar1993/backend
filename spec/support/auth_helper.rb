module AuthHelper
  def http_login
    device = create(:device)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(device.uid)
  end
end
