module FcmHelper
  def stub_fcm_notifications
    stub_request(:post, "https://fcm.googleapis.com/fcm/send")
      .to_return(status: 200, body: "", headers: {} )
  end
end
