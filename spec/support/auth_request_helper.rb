module AuthRequestHelper
  #
  # pass the @env along with your request, eg:
  #
  # GET '/labels', {}, @env
  #
  def http_login
    @env ||= {}
    device = create(:device)
    @token = ActionController::HttpAuthentication::Token.encode_credentials(device.uid)
    @env['HTTP_AUTHORIZATION'] = @token
  end
end
