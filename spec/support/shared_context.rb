RSpec.shared_context 'videos in multiple categories', shared_context: :metadata do
  let!(:video_category1) { create :video_category }
  let!(:video_category2) { create :video_category }
  let!(:video1) { create :video, video_category: video_category1 }
  let!(:video2) { create :video, video_category: video_category2 }
end

RSpec.configure do |rspec|
  rspec.include_context 'videos in multiple categories', include_shared: true
end
