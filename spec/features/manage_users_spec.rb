require 'rails_helper'

describe 'Manage Users' do
  before do
    @user = create(:user)
    @user.name = 'John Doe'
    @user.country = 'India'
    @user.save!
  end
  scenario 'Admin user can manage users' do
    page.driver.browser.basic_authorize('blacklotus', 'blacklotus')
    visit '/admin'
    expect(page).to have_link 'Users'
    click_on 'Users'
    # should have total users count
    expect(page).to have_content I18n.t('users.admin_panel_user_count')
    expect(page).to have_css('span#user-count', text: '1')
    # should have total blacklisted users count
    expect(page).to have_content I18n.t('users.admin_panel_blacklisted_count')
    expect(page).to have_css('span#blacklist-count', text: '0')
    # should display user name
    expect(page).to have_content @user.name
    # should dispaly user email
    expect(page).to have_content @user.name
    # should display user country
    expect(page).to have_content @user.country
    # should display blacklisted status
    expect(page).to have_content @user.blacklisted?.to_s
    # should dipslay edit and destroy links
    expect(page).to have_link I18n.t('administrate.actions.edit')
    expect(page).to have_link I18n.t('administrate.actions.destroy')
    # blacklist a user
    click_on I18n.t('administrate.actions.edit')
    page.check 'Blacklisted'
    click_on 'Update User'
    expect(page).to have_content 'User was successfully updated.'
    expect(@user.reload.blacklisted?).to be true
    click_on 'Users'
    expect(page).to have_css('span#user-count', text: '1')
    expect(page).to have_css('span#blacklist-count', text: '1')
  end
end
