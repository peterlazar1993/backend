require 'rails_helper'

describe 'Manage Global Mediatations' do
  before :all do
    @schduled_at = 1.day.from_now
    @file = File.absolute_path('spec/support/fixtures/global.mp3')
  end
  scenario 'Admin user can manage global mediatations' do
    page.driver.browser.basic_authorize('blacklotus', 'blacklotus')
    visit '/admin'
    # Check correct admin navigation links are present
    expect(page).to have_link 'Categories'
    expect(page).to have_link 'Private Meditations'
    expect(page).to have_link 'Global Meditations'
    expect(page).to have_link 'Swaminars'
    # admin root path is global mediations
    expect(page).to have_selector(:link_or_button, 'New global meditation')
    click_on 'New global meditation'
    # should be able to go back
    expect(page).to have_selector(:link_or_button, 'Back')
    fill_in :global_meditation_scheduled_at,
            with: @schduled_at.strftime('%Y-%m-%d %H:%M:%S')
    attach_file('global_meditation_track', @file)
    click_on 'Create Global meditation'
    expect(page).to have_content 'GlobalMeditation was successfully created'
  end
end
