require 'rails_helper'

describe 'Manage Swaminars' do
  scenario 'Admin user can manage swaminars' do
    page.driver.browser.basic_authorize('blacklotus', 'blacklotus')
    visit '/admin'
    expect(page).to have_link 'Swaminars'
    click_on 'Swaminars'
    # admin root path is global mediations
    expect(page).to have_selector(:link_or_button, t('swaminar.new_quick_swaminar'))
    expect(page).to have_selector(:link_or_button, t('swaminar.new_custom_swaminar'))
  end
end
