require 'rails_helper'
require 'youtube_api/paginated_response'

module YoutubeApi
  RSpec.describe PaginatedResponse do
    let(:dummy_category) { build_stubbed :video_category }
    let(:paginated_response) { PaginatedResponse.from_json '{"nextPageToken": "page_token"}' }
    let(:paginated_response_last_page) { PaginatedResponse.from_json '{}' }

    describe '#last_page?' do
      it 'returns true if it is the last page' do
        expect(paginated_response_last_page).to be_last_page
      end

      it 'returns false if it is not the last page' do
        expect(paginated_response).not_to be_last_page
      end
    end

    describe '#next_page_token' do
      it 'returns the next page token' do
        expect(paginated_response.next_page_token).to eql 'page_token'
      end

      it 'returns nil if it is the last page' do
        expect(paginated_response_last_page.next_page_token).to be_nil
      end
    end

    describe '#video_hashes' do
      let(:response_json) { file_fixture('youtube_api_response/channel_1_page_1.json').read }

      let(:paginated_response) { PaginatedResponse.from_json response_json }

      it 'returns a collection of video hashes' do
        video_hashes = paginated_response.video_hashes

        expect(video_hashes.size).to eql 2
        video_hashes.each_with_index do |video_hash, index|
          expect(video_hash[:title]).to eql "title#{index}"
          expect(video_hash[:description]).to eql "description#{index}"
          expect(video_hash[:youtube_video_id]).to eql "videoId#{index}"
          expect(video_hash[:thumbnail_url]).to eql "http://i.ymtg.com/vi/#{index}.jpg"
          expect(video_hash[:published_at]).to eql '2017-11-28T18:05:30.000Z'
        end
      end

      it 'returns a collection that can be used to build valid Video records' do
        paginated_response.video_hashes.each do |video_hash|
          video_attributes = video_hash.merge(video_category: dummy_category)
          expect(Video.new(video_attributes)).to be_valid
        end
      end
    end
  end
end
