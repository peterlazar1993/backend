require 'rails_helper'
require 'youtube_api/client'
require 'youtube_api/paginated_response'

module YoutubeApi
  RSpec.describe Client do
    describe '#get_videos' do
      let(:client) { Client.new api_key: 'some_api_key' }
      let(:stubbed_response) { '{}' }
      let(:channel_id) { 'some_channel' }

      it 'makes a call to Youtube API to search for videos' do
        allow(RestClient).to receive(:get)
          .with('https://www.googleapis.com/youtube/v3/search',
                params: {
                  part: 'snippet',
                  order: 'date',
                  type: 'video',
                  maxResults: 50,
                  pageToken: '',
                  channelId: channel_id,
                  key: 'some_api_key'
                }).and_return stubbed_response

        response = client.get_videos channel_id: channel_id

        expect(response).to be_a PaginatedResponse
      end
    end
  end
end
