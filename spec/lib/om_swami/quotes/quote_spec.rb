# frozen_string_literal: true

require 'uri'

module OmSwami
  module Quotes
    RSpec.describe Quote do
      let(:stubbed_response) { File.read('spec/data/om_swami_quote_feed.rss') }
      let(:quote_object) do
        OmSwami::Quotes::Quote.new(
          RSS::Parser.parse(stubbed_response, false).channel
        )
      end
      let(:quote_of_the_day) do
        'Sometimes in wanting too much, you lose what you have.'
      end

      describe '#to_h' do
        it 'should be convert the object to hash' do
          expect(quote_object.to_h).to be_a Hash
        end

        it 'should have correct date' do
          expect(quote_object.to_h[:date]).to eq Date.current
        end
      end

      describe 'prviate methods' do
        describe '#content' do
          it 'should equal ' do
            expect(quote_object.send(:content)).to eq(quote_of_the_day)
          end
        end
      end
    end
  end
end
