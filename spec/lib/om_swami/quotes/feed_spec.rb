# frozen_string_literal: true

require 'rails_helper'

module OmSwami
  module Quotes
    RSpec.describe Feed do
      describe '#fetch' do
        let(:feed_data) { OmSwami::Quotes::Feed.fetch }
        let(:stubbed_response) { File.read('spec/data/om_swami_quote_feed.rss') }
        let(:attributes) do
          %i[content date]
        end

        before(:each) do
          allow(RSS::Parser).to receive(:parse)
            .with(OmSwami::Quotes::Feed::URL, false)
            .and_return RSS::Parser.parse(stubbed_response, false)
        end

        it 'should return a Hash' do
          expect(feed_data).to be_a Hash
        end

        it 'should have hash with the following keys' do
          expect(feed_data.keys).to eq(attributes)
        end

        it 'should have correct date for quote' do
          expect(feed_data[:date]).to eq Date.current
        end
      end
    end
  end
end
