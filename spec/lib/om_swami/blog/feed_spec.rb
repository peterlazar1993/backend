# frozen_string_literal: true

require 'om_swami/blog/feed'
require 'rails_helper'

module OmSwami
  module Blog
    RSpec.describe Feed do
      describe '#fetch' do
        let(:feed_data) { OmSwami::Blog::Feed.fetch }
        let(:stubbed_response) { File.read('spec/data/om_swami_feed.rss') }
        let(:attributes) do
          %i[title published_at tags cover_image link summary]
        end

        before(:each) do
          allow(RSS::Parser).to receive(:parse)
            .with(OmSwami::Blog::Feed::URL)
            .and_return RSS::Parser.parse(stubbed_response)
        end

        it 'should return an Array of Hash' do
          expect(feed_data).to be_a Array
          expect(feed_data.first).to be_a Hash
        end

        it 'should have hash with the following keys' do
          expect(feed_data.first.keys).to eq(attributes)
        end
      end
    end
  end
end
