# frozen_string_literal: true

require 'om_swami/blog/feed'
require 'uri'

module OmSwami
  module Blog
    RSpec.describe Article do
      let(:sample_response) { File.read('spec/data/om_swami_feed.rss'); }
      let(:article_object) do
        OmSwami::Blog::Article.new(
          RSS::Parser.parse(sample_response).items.first
        )
      end

      describe '#to_h' do
        it 'should be convert the object to hash' do
          expect(article_object.to_h).to be_a Hash
        end
      end

      describe 'prviate methods' do
        let(:sample_response) { File.read('spec/data/om_swami_feed.rss'); }
        let(:article_object) do
          OmSwamiBlog::Post.new(RSS::Parser.parse(sample_response).items.first)
        end
        let(:first_line_of_post) do
          'Has it ever happened to you that you are unable to shake of'
        end
        let(:book_identifier) do
          '<div style="background-color: #f0f8ff;padding: 20px;font-family: \'Verdana\';font-size: 90%;line-height: 1.5em">'
        end

        describe '#title' do
          it 'should equal ' do
            expect(article_object.send(:title)).to eq('The Tearing Thought')
          end
        end

        describe '#summary' do
          it 'should end with the end of the sentance' do
            expect(article_object.send(:summary)[-1]).to eq('.')
          end
        end

        describe '#published' do
          it 'should return a time object' do
            expect(article_object.send(:published_at)).to be_a Time
          end
        end

        describe '#cover_image' do
          it 'should return a url of the coverage image' do
            expect(article_object.send(:cover_image)).to match(URI::DEFAULT_PARSER.make_regexp)
          end
        end
      end
    end
  end
end
