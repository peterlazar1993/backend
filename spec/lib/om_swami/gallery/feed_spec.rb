# frozen_string_literal: true

require 'rails_helper'

module OmSwami
  module Gallery
    RSpec.describe Feed do
      before do
        allow_any_instance_of(OmSwami::Gallery::Images)
          .to receive(:fetch_images)
          .and_return([GalleryImage.new(url: 'http://image.jpg')])
      end

      describe '#fetch' do
        let(:feed_data) { OmSwami::Gallery::Feed.fetch }
        let(:stubbed_response) { File.read('spec/data/om_swami_gallery_feed.rss') }
        let(:attributes) do
          %i[name date link cover_image images]
        end

        before(:each) do
          allow(RSS::Parser).to receive(:parse)
            .with(OmSwami::Gallery::Feed::URL)
            .and_return RSS::Parser.parse(stubbed_response)
        end

        it 'should return an Array of Hash' do
          expect(feed_data).to be_a Array
          expect(feed_data.first).to be_a Hash
        end

        it 'should have hash with the following keys' do
          expect(feed_data.first.keys).to eq(attributes)
        end
      end
    end
  end
end
