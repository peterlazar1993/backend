# frozen_string_literal: true

module OmSwami
  module Gallery
    RSpec.describe Gallery do
      let(:stubbed_response) { File.read('spec/data/om_swami_gallery_feed.rss') }
      let(:gallery_object) do
        OmSwami::Gallery::Gallery.new(
          RSS::Parser.parse(stubbed_response).items.first
        )
      end
      let(:first_gallery_name) do
        'Annual Celebrations March 2017'
      end

      before do
        allow_any_instance_of(OmSwami::Gallery::Images)
          .to receive(:fetch_images)
          .and_return([GalleryImage.new(url: 'http://image.jpg')])
        stub_fcm_notifications
      end

      describe '#to_h' do
        it 'should be convert the object to hash' do
          expect(gallery_object.to_h).to be_a Hash
        end
      end

      describe 'prviate methods' do
        describe '#name' do
          it 'should equal ' do
            expect(gallery_object.send(:name)).to eq(first_gallery_name)
          end
        end

        describe '#date' do
          it 'should return a time object' do
            expect(gallery_object.send(:date)).to be_a Time
          end
        end

        describe '#gallery_link' do
          it 'should return correct book url' do
            expect(gallery_object.send(:gallery_link))
              .to match(URI::DEFAULT_PARSER.make_regexp)
          end
        end

        describe '#cover_image' do
          it 'should return a url of the coverage image' do
            expect(gallery_object.send(:cover_image))
              .to match(URI::DEFAULT_PARSER.make_regexp)
          end
        end

        describe '#gallery_images' do
          let(:gallery) { create(:gallery_with_images) }
          before do
            allow_any_instance_of(Gallery).to receive(:name)
              .and_return gallery.name
            allow_any_instance_of(Gallery).to receive(:date)
              .and_return gallery.date
          end

          it 'should return [] if gallery already exists' do
            expect(gallery_object.send(:gallery_images)).to eq []
          end
        end
      end
    end
  end
end
