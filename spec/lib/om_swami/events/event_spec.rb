# frozen_string_literal: true

module OmSwami
  module Events
    RSpec.describe Event do
      let(:stubbed_response) { File.read('spec/data/om_swami_events_feed.rss') }
      let(:event_object) do
        OmSwami::Events::Event.new(
          RSS::Parser.parse(stubbed_response).items.first
        )
      end
      let(:first_event_title) do
        'Gayatri Sadhana Camp. Bangalore. Feb 2 – 4. Full.'
      end

      describe '#to_h' do
        it 'should be convert the object to hash' do
          expect(event_object.to_h).to be_a Hash
        end
      end

      describe 'prviate methods' do
        describe '#title' do
          it 'should equal ' do
            expect(event_object.send(:title)).to eq(first_event_title)
          end
        end

        describe '#scheduled_at' do
          let(:scheduled_at) { event_object.send(:scheduled_at) }

          it 'should be an iso8601 datetime' do
            expect(scheduled_at).to eq scheduled_at.to_time.iso8601
          end
        end

        describe '#description' do
          let(:description) do
            Nokogiri::HTML.parse(event_object.send(:description)).text
          end

          it 'should return event description correctly' do
            expect(event_object.send(:description)).to eq description
          end
        end

        describe '#link' do
          it 'should return correct book url' do
            expect(event_object.send(:link))
              .to match(URI::DEFAULT_PARSER.make_regexp)
          end
        end
      end
    end
  end
end
