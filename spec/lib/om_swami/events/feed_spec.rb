# frozen_string_literal: true

require 'rails_helper'

module OmSwami
  module Events
    RSpec.describe Feed do
      describe '#fetch' do
        let(:feed_data) { OmSwami::Events::Feed.fetch }
        let(:stubbed_response) { File.read('spec/data/om_swami_events_feed.rss') }
        let(:attributes) do
          %i[title scheduled_at description link]
        end

        before(:each) do
          allow(RSS::Parser).to receive(:parse)
            .with(OmSwami::Events::Feed::URL)
            .and_return RSS::Parser.parse(stubbed_response)
        end

        it 'should return an Array of Hash' do
          expect(feed_data).to be_a Array
          expect(feed_data.first).to be_a Hash
        end

        it 'should have hash with the following keys' do
          expect(feed_data.first.keys).to eq(attributes)
        end
      end
    end
  end
end
