# frozen_string_literal: true

module OmSwami
  module Books
    RSpec.describe Book do
      let(:stubbed_response) { File.read('spec/data/om_swami_book_feed.rss') }
      let(:book_object) do
        OmSwami::Books::Book.new(
          RSS::Parser.parse(stubbed_response).items.first
        )
      end
      let(:first_tile) { 'A Fistful of Wisdom' }

      describe '#to_h' do
        it 'should be convert the object to hash' do
          expect(book_object.to_h).to be_a Hash
        end
      end

      describe 'prviate methods' do
        describe '#title' do
          it 'should equal ' do
            expect(book_object.send(:title)).to eq(first_tile)
          end
        end

        describe '#published_at' do
          it 'should return a time object' do
            expect(book_object.send(:published_at)).to be_a Time
          end
        end

        describe '#cover_image' do
          it 'should return a url of the coverage image' do
            expect(book_object.send(:cover_image))
              .to match(URI::DEFAULT_PARSER.make_regexp)
          end
        end

        describe '#link' do
          it 'should return correct book url' do
            expect(book_object.send(:link))
              .to match(URI::DEFAULT_PARSER.make_regexp)
          end
        end
      end
    end
  end
end
