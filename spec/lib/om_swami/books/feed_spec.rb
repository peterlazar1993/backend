# frozen_string_literal: true

require 'rails_helper'

module OmSwami
  module Books
    RSpec.describe Feed do
      describe '#fetch' do
        let(:feed_data) { OmSwami::Books::Feed.fetch }
        let(:stubbed_response) { File.read('spec/data/om_swami_book_feed.rss') }
        let(:attributes) do
          %i[title published_at description cover_image link]
        end

        before(:each) do
          allow(RSS::Parser).to receive(:parse)
            .with(OmSwami::Books::Feed::URL)
            .and_return RSS::Parser.parse(stubbed_response)
        end

        it 'should return an Array of Hash' do
          expect(feed_data).to be_a Array
          expect(feed_data.first).to be_a Hash
        end

        it 'should have hash with the following keys' do
          expect(feed_data.first.keys).to eq(attributes)
        end
      end
    end
  end
end
