# frozen_string_literal: true

require 'om_swami/feed'
require 'rails_helper'

module OmSwami
  RSpec.describe Feed do
    describe '#fetch' do
      let(:error_message) { 'Fetch not implmented in child class' }

      it 'should raise an error as the method is not
          supposed to be run directly' do
        expect { OmSwami::Feed.fetch }.to raise_error(RuntimeError, error_message)
      end
    end
  end
end
