ENV["RACK_ENV"] = "test"

require File.expand_path("../../config/environment", __FILE__)
abort("DATABASE_URL environment variable is set") if ENV["DATABASE_URL"]

require "rspec/rails"
require "paperclip/matchers"
require "webmock/rspec"
Dir[Rails.root.join("spec/support/**/*.rb")].sort.each { |file| require file }

module Features
  # Extend this module in spec/support/features/*.rb
  include Formulaic::Dsl
end

RSpec.configure do |config|
  config.include Features, type: :feature
  config.include Paperclip::Shoulda::Matchers
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include AuthHelper, type: :controller
  config.include AuthRequestHelper, type: :request
  config.include FcmHelper

  config.infer_base_class_for_anonymous_controllers = false
  config.infer_spec_type_from_file_location!
  config.use_transactional_fixtures = false
  config.before(:each) do
    allow_any_instance_of(PrivateMeditation).to receive(:notify_users).and_return("")
    allow_any_instance_of(GlobalMeditation).to receive(:notify_users).and_return("")
  end
  config.before(:each, type: :system) do
    driven_by :rack_test
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.after(:suite) do
    FileUtils.rm_rf(Dir["#{Rails.root}/spec/test_files/"])
  end
end

ActiveRecord::Migration.maintain_test_schema!
