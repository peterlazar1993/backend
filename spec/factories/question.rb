FactoryBot.define do
  factory :question do
    body 'Question content'
    user
    factory :swaminar_question do
      askable { |a| a.association(:swaminar) }
    end
  end
end
