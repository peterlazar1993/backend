FactoryBot.define do
  factory :swaminar do
    broadcast_slug 'abc'
    stream_id 'cde'
    factory :swaminar_with_attendees do
      transient do
        attendees_count 3
      end
      after(:create) do |swaminar, evaluator|
        create_list(:swaminar_attendee,
                    evaluator.attendees_count,
                    swaminar: swaminar)
      end
    end
  end
end
