FactoryBot.define do
  factory :gallery do
    sequence(:name){ |n| "Name #{n}" }
    link 'www.example.com'
    date Date.current
    cover_image 'https://my_image.jpeg'

    factory :gallery_with_images do
      after(:create) do |gallery|
        create(:gallery_image, gallery: gallery)
      end
    end
  end

  factory :gallery_image do
    url 'http://sample_image.jpg'
  end

  factory :quote do
    content "Today's quote"
    date Date.current
  end

  factory :event do
    sequence :title do |n|
      "title #{n}"
    end
    scheduled_at Time.current
    description 'Event description'
    link 'https://omswami.org/event/gayatri-sadhana-camp-bangalore-feb-2-4'
  end

  factory :book do
    sequence :title do |n|
      "title #{n}"
    end
    published_at Time.current
    description 'Book description'
    cover_image 'http://omswami.com/wp-content/uploads/2017/05/TASM-Cover-659x1024.jpg'
    link 'http://omswami.com/book/ancient-science-mantras'
  end

  factory :article do
    sequence :title do |n|
      "title #{n}"
    end
    cover_image 'http://omswami.com/wp-content/uploads/2018/01/TearingThought-1024x747.jpg'
    link 'http://omswami.com/2018/01/the-tearing-thought.html'
    published_at Time.current
    summary 'Summary has to be 30 character long'
    tags 'Meditation, Wisdom'
  end

  factory :video do
    title "title"
    description "description"
    sequence :youtube_video_id do |n|
      "videoId#{n}"
    end
    thumbnail_url "https://i.ytimg.com/vi/videoId/mqdefault.jpg"
    sequence :published_at do |n|
      DateTime.now + n.minutes
    end
    video_category nil

    factory :video_with_category do
      association :video_category
    end
  end
  factory :favorite
  factory :video_category do
    name "Name"
    description "description"
    sequence :youtube_channel_id do |n|
      "channelId#{n}"
    end
  end
  factory :global_meditation_session, class: Session do
    played_at "2017-10-25 16:33:57"
    meditation_type "global"
    meditation_type_id 1
    duration_in_minutes 10
  end
  factory :private_meditation_with_guided_track,class: PrivateMeditation do
    name "Compassion#1"
    guided_track { File.new("#{Rails.root}/spec/support/fixtures/guided.mp3") }
    association :category
  end
  factory :private_meditation_with_music_track,class: PrivateMeditation do
    name "Compassion#2"
    music_track { File.new("#{Rails.root}/spec/support/fixtures/music.mp3") }
    association :category
  end
  factory :private_meditation_with_both_tracks,class: PrivateMeditation do
    name "Compassion#3"
    guided_track { File.new("#{Rails.root}/spec/support/fixtures/guided.mp3") }
    music_track { File.new("#{Rails.root}/spec/support/fixtures/music.mp3") }
    association :category
  end
  factory :category do
    name "Compassion"
    count_of_meditations 3
  end
  factory :device do
    device_id "a66d08b4-8013-46c4-9125-3d656b9f1b7e"
    platform "android"
    push_token "d29594819a503437070699e03f7093318419dcdd39ba554776b160500793ff45"
  end
  factory :global_meditation_participant do
    association :global_meditation
    association :user
  end
  factory :user do
    sequence :email do |n|
      "user#{n}@email.com"
    end
    password 'password'
    sequence(:name) { |n| "user #{n}" }
    sequence(:country) { |n| "#{n}country" }
  end
  factory :global_meditation do
    scheduled_at 1.week.from_now
    track { File.new("#{Rails.root}/spec/support/fixtures/global.mp3") }
  end
end
