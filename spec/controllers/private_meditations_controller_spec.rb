require 'rails_helper'

RSpec.describe PrivateMeditationsController, type: :controller do
  context 'with valid device' do
    before do
      http_login
    end
    describe "GET #index" do
      it "returns all private meditations" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "Favorite/Unfavorite" do
      let(:user) { create(:user) }
      let(:private_meditation) { create(:private_meditation_with_both_tracks) }

      describe "Favorite " do
        it "Valid user" do
          request.headers.merge! user.create_new_auth_token
          post :favorite, params: {id: private_meditation.id}, format: :json
          expect(response).to have_http_status(:created)
          expect(Favorite.count).to eq 1
          expect(Favorite.last.favorable).to eq(private_meditation)
        end

        it "Ignore for invalid user" do
          post :favorite, params: {id: private_meditation.id}, format: :json
          expect(response).to have_http_status(:unauthorized)
          expect(Favorite.count).to eq 0
        end

        it "Ignore if already favorited" do
          request.headers.merge! user.create_new_auth_token
          post :favorite,params: {id: private_meditation.id}, format: :json
          post :favorite, params: {id: private_meditation.id}, format: :json
          expect(Favorite.count).to eq 1
        end
      end

      describe "Unfavorite" do
        let!(:favorite) {Favorite.create(
          user: user,
          favorable: private_meditation
        )}

        it "Valid user" do
          favorite_count = Favorite.count
          request.headers.merge! user.create_new_auth_token
          post :unfavorite, params: {id: private_meditation.id}, format: :json
          expect(response).to have_http_status(:no_content)
          expect(Favorite.count).to eq 0
        end

        it "Ignore for invalid user" do
          post :favorite, params: {id: private_meditation.id}, format: :json
          expect(response).to have_http_status(:unauthorized)
          expect(Favorite.count).to eq 1
        end
      end
    end
  end
end
