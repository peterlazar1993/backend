require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  describe 'GET #index' do
    before do
      http_login
    end

    context 'with valid device token' do
      it 'returns all the categories' do
        get :index
        expect(response).to have_http_status(:success)
      end

      it "Return 'all' as a category" do
        category = create(:category)
        get :index
        json_response = JSON.parse(response.body)
        all_category = json_response[0]
        expect(all_category['id']).to eq Category::ALL_CATEGORY_ID
        expect(all_category['name']).to eq Category::ALL_CATEGORY_NAME
        expect(json_response[1]['name']).to eq category.name
        expect(json_response.length).to eq 2
      end
    end
  end
end
