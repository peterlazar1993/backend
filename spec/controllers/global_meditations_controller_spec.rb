require 'rails_helper'
require 'spec_helper'

RSpec.describe GlobalMeditationsController, type: :controller do
  context 'with valid device' do

    before do
      http_login
    end

    describe 'GET #index' do
      it 'returns  all global meditations' do
        get :index
        expect(response).to have_http_status(:success)
      end

      describe 'params[:elapsed]' do
        before do
          @elapsed = create(:global_meditation, scheduled_at: 2.days.ago)
          @elapsed_on_same_day = create(:global_meditation, scheduled_at: 5.minutes.ago)
          @upcoming = create(:global_meditation, scheduled_at: 3.days.from_now)
        end

        context 'when true' do
          it 'should return only elapsed global meditations' do
            get :index, as: :json, params: { elapsed: true }
            http_response = JSON.parse(response.body)
            expect(http_response.size).to eq 2
            expect(http_response.first['scheduled_at'])
              .to eq @elapsed.scheduled_at.iso8601
            expect(http_response.last['scheduled_at'])
              .to eq @elapsed_on_same_day.scheduled_at.iso8601
          end
        end

        context 'when unspecified' do
          it 'should return only upcoming global meditations' do
            get :index
            http_response = JSON.parse(response.body)
            expect(http_response.size).to eq 1
            expect(http_response.first['scheduled_at'])
              .to eq @upcoming.scheduled_at.iso8601
          end
        end
      end

      context 'pagination' do
        it 'should have default per page 10 items' do
          create_list(:global_meditation, 11)
          get :index
          expect(JSON.parse(response.body).size).to eq 10
        end
      end
    end

    describe "Join/Unjoin a meditation" do
      let(:user) { create(:user) }
      let(:global_meditation) { create(:global_meditation) }
      let(:latitude) {20.593684}
      let(:longitude) {78.96288}

      describe "Join Meditation" do
        include_context 'stub geocode api'
        it "Valid user" do
          request.headers.merge! user.create_new_auth_token
          post "join", params: {id: global_meditation.id, latitude: latitude, longitude: longitude}, format: :json
          expect(response).to have_http_status(:created)
          expect(GlobalMeditationParticipant.count).to eq 1
        end

        it "Identify the location for the participant from the lat/long" do
          request.headers.merge! user.create_new_auth_token
          post "join", params: {id: global_meditation.id, latitude: latitude, longitude: longitude}, format: :json
          participant = GlobalMeditationParticipant.last
          expect(participant.ip_address).not_to be_nil
          json_response = JSON.parse(response.body)
          coordinates = json_response["coordinates"]
          expect([coordinates[0].round(5),coordinates[1].round(5)]).to match_array(
            [participant.longitude.round(5),participant.latitude.round(5)]
          )
        end

        it "Ignore the location if no lat/long present" do
          request.headers.merge! user.create_new_auth_token
          post "join", params: {id: global_meditation.id}, format: :json
          participant = GlobalMeditationParticipant.last
          expect(participant.address).to be_nil
        end

        it "Ignore if already joined " do
          request.headers.merge! user.create_new_auth_token
          post "join", params: {id: global_meditation.id, latitude: latitude, longitude: longitude}, format: :json
          expect(GlobalMeditationParticipant.count).to eq 1
          post "join", params: {id: global_meditation.id, latitude: latitude, longitude: longitude}, format: :json
          expect(GlobalMeditationParticipant.count).to eq 1
        end

        it "Ignore if Invalid user" do
          post "join", params: {id: global_meditation.id, latitude: latitude, longitude: longitude}, format: :json
          expect(response).to have_http_status(:unauthorized)
          expect(GlobalMeditationParticipant.count).to eq 0
        end
      end

      describe "Unjoin" do
        let!(:participant) {GlobalMeditationParticipant.create(user: user, global_meditation: global_meditation)}

        it "Valid user" do
          request.headers.merge! user.create_new_auth_token
          post "unjoin", params: {id: global_meditation.id}, format: :json
          expect(response).to have_http_status(:no_content)
          expect(GlobalMeditationParticipant.count).to eq 0
        end

        it "Invalid User" do
          post "join", params: {id: global_meditation.id}, format: :json
          expect(response).to have_http_status(:unauthorized)
          expect(GlobalMeditationParticipant.count).to eq 1
        end
      end
    end
  end
end
