require 'rails_helper'

RSpec.describe VideosController, type: :controller do

  describe "GET #index" do
    it "returns a success response" do
      http_login
      get :index, params: {}
      expect(response).to be_success
    end
  end
end
