require 'rails_helper'

RSpec.describe DevicesController, type: :controller do

  describe "Register device" do
    let(:device) { build(:device) }
    let(:device_params) { device.attributes.except('uid', 'id', 'created_at', 'updated_at','user_id') }

    it "Respond with created status" do
      post :create, params: { device: device_params }, format: :json
      expect(response).to have_http_status(:created)
      expect(Device.count).to eq 1
    end

    it "Ignore if already exists" do
      post :create, params: { device: device_params }, format: :json
      post :create, params: { device: device_params }, format: :json
      expect(Device.count).to eq 1
    end
  end
end
