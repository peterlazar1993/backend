# frozen_string_literal: true

require 'rails_helper'
require 'youtube_api/o_auth_client'
require 'google/apis/youtube_v3'

RSpec.describe SwaminarsController, type: :controller do
  include_context 'stub youtube api'

  before do
    stub_fcm_notifications
  end

  describe 'GET #index' do
    context 'without device identification token' do
      it 'should not allow end-point access' do
        get :index
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'with valid device identification token' do
      let(:device) { create(:device) }
      let(:token) do
        ActionController::HttpAuthentication::Token.encode_credentials(device.uid)
      end

      it 'should allow end-point access' do
        request.env['HTTP_AUTHORIZATION'] = token
        get :index
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'Join/Unjoin a swaminar' do
    let(:user) { create(:user) }
    let(:swaminar) { create(:swaminar) }
    let(:latitude) { 20.593684 }
    let(:longitude) { 78.96288 }

    describe 'Join Swaminar' do
      include_context 'stub geocode api'
      context 'with location info' do
        before do
          request.headers.merge! user.create_new_auth_token
          post 'join', params: { id: swaminar.id, latitude: latitude,
            longitude: longitude }, format: :json
        end

        it 'should allow a valid user to join' do
          expect(response).to have_http_status(:created)
          expect(swaminar.attendees.count).to eq 1
        end

        it 'Identify the location for the attendee from the lat/long' do
          attendee = swaminar.attendees.last
          expect(attendee.ip_address).not_to be_nil
          json_response = JSON.parse(response.body)
          coordinates = json_response['coordinates']
          expect([coordinates[0].round(5), coordinates[1].round(5)]).
            to match_array(
              [attendee.longitude.round(5), attendee.latitude.round(5)]
            )
        end

        it 'should ignore attendee if already joined' do
          expect(swaminar.attendees.count).to eq 1
          post 'join', params: { id: swaminar.id, latitude: latitude,
            longitude: longitude }, format: :json
          expect(swaminar.attendees.count).to eq 1
        end
      end

      context 'without location info' do
        it 'should ignore attendee location if no lat/long present' do
          request.headers.merge! user.create_new_auth_token
          post 'join', params: { id: swaminar.id }, format: :json
          attendee = swaminar.attendees.last
          expect(attendee.address).to eq nil
        end
      end

      it 'should ignore invalid users' do
        post 'join', params: { id: swaminar.id, latitude: latitude,
          longitude: longitude }, format: :json
        expect(response).to have_http_status(:unauthorized)
        expect(swaminar.attendees.count).to eq 0
      end
    end

    describe 'Unjoin' do
      before do
        SwaminarAttendee.create(user: user, swaminar: swaminar)
      end

      it 'should unjoin a valid user' do
        request.headers.merge! user.create_new_auth_token
        post 'unjoin', params: { id: swaminar.id }, format: :json
        expect(response).to have_http_status(:no_content)
        expect(swaminar.attendees.count).to eq 0
      end

      it 'should not allow unauthorized users' do
        post 'join', params: { id: swaminar.id }, format: :json
        expect(response).to have_http_status(:unauthorized)
        expect(swaminar.attendees.count).to eq 1
      end
    end
  end
end
