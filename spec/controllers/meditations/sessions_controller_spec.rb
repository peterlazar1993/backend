require 'rails_helper'

RSpec.describe Meditations::SessionsController, type: :controller do

  describe 'Meditation Session' do
    describe 'Stats' do
      let(:session) { build(:global_meditation_session) }
      let(:user) { create(:user) }
      let(:session_params) do
        session.attributes.except('id', 'created_at', 'updated_at', 'user_id')
      end
      let(:feedback_params) do
        { notes: 'useful session',
          'ratings': {
            'mindfulness': 'good', 'mood': 'good'
          }
        }
      end

      it 'Save' do
        request.headers.merge! user.create_new_auth_token
        post :create,
             params: { session: session_params, feedback: feedback_params },
             format: :json
        expect(response).to have_http_status(:created)
        json_response = JSON.parse(response.body)
        session = Session.last
        expect(json_response['session']['id']).to be session.id
        expect(session.feedback).not_to be_nil
      end

      it 'Invalid parameters' do
        request.headers.merge! user.create_new_auth_token
        post :create, params: { session: { played_at: Time.now, meditation_type: '' } }, format: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'Invalid User' do
        post :create, params: {session: session_params}, format: :json
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
