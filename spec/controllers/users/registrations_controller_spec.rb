require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do

  describe 'Email authentication' do
    it "Respond with created status" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, params: {email: 'email@email.com', password: 'password', name: 'Name', image: 'image_url', country: 'USA' }
      expect(response).to have_http_status(:success)
    end

    it 'responds with created user object' do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, params: {email: 'email@email.com', password: 'password', name: 'Name', image: 'image_url', country: 'USA' }
      json_response = JSON.parse(response.body)
      expect(json_response['uid']).to eq 'email@email.com'
      expect(json_response['email']).to eq 'email@email.com'
      expect(json_response['name']).to eq 'Name'
      expect(json_response['image']).to eq 'image_url'
      expect(json_response['provider']).to eq 'email'
    end
  end
end
