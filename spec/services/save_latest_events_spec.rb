# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaveLatestEvents do
  describe '#execute' do
    let(:sample_response) { File.read('spec/data/om_swami_events_feed.rss') }

    before(:each) do
      allow(RSS::Parser).to receive(:parse)
        .with(OmSwami::Events::Feed::URL)
        .and_return RSS::Parser.parse(sample_response)
      stub_fcm_notifications
    end

    it 'should save latest blog post to database' do
      described_class.execute
      expect(Event.count).to eq(8)
    end

    it 'should not duplicate the already saved blog post' do
      described_class.execute
      described_class.execute
      expect(Event.count).to eq(8)
    end
  end
end
