# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe UpdateLiveStream do
  let(:id) { 'Qzhm5VxYqz0' }
  let(:title) { 'title' }
  let(:description) { 'description' }
  let(:part) { 'id,snippet,status' }
  let(:subject) { described_class.new(id, title, description) }
  let(:namespace) { described_class::YT }

  include_context 'stub youtube api'

  describe '#execute' do
    it 'should update broadcast data in youtube' do
      expect_any_instance_of(namespace::YouTubeService).
        to receive(:update_live_stream)
      subject.execute
    end
  end
end
