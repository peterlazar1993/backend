# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe UpdateBroadcast do
  let(:id) { 'Qzhm5VxYqz0' }
  let(:title) { 'title' }
  let(:description) { 'description' }
  let(:start_time) { 'start time' }
  let(:part) { 'id,snippet,status' }
  let(:subject) { described_class.new(id, title, description, start_time) }
  let(:namespace) { described_class::YT }

  include_context 'stub youtube api'

  describe '#execute' do
    it 'should update broadcast data in youtube' do
      expect_any_instance_of(namespace::YouTubeService).
        to receive(:update_live_broadcast)
      subject.execute
    end
  end
end
