# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaveLatestBooks do
  describe '#execute' do
    let(:sample_response) { File.read('spec/data/om_swami_book_feed.rss'); }

    before(:each) do
      allow(RSS::Parser).to receive(:parse)
        .with(OmSwami::Books::Feed::URL)
        .and_return RSS::Parser.parse(sample_response)
      stub_fcm_notifications
    end

    it 'should save latest blog post to database' do
      described_class.execute
      expect(Book.count).to eq(10)
    end

    it 'should not duplicate the already saved blog post' do
      described_class.execute
      described_class.execute
      expect(Book.count).to eq(10)
    end
  end
end
