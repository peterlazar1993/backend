# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaveLatestGallery do
  describe '#execute' do
    let(:sample_response) { File.read('spec/data/om_swami_gallery_feed.rss') }

    before do
      allow_any_instance_of(OmSwami::Gallery::Images)
        .to receive(:fetch_images)
        .and_return([GalleryImage.new(url: 'http://image.jpg')])
      stub_fcm_notifications
    end

    before(:each) do
      allow(RSS::Parser).to receive(:parse)
        .with(OmSwami::Gallery::Feed::URL)
        .and_return RSS::Parser.parse(sample_response)
    end

    it 'should save latest blog post to database' do
      described_class.execute
      expect(Gallery.count).to eq(10)
    end

    it 'should not duplicate the already saved blog post' do
      described_class.execute
      described_class.execute
      expect(Gallery.count).to eq(10)
    end
  end
end
