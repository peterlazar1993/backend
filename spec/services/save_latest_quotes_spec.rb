# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaveLatestQuotes do
  describe '#execute' do
    let(:sample_response) { File.read('spec/data/om_swami_quote_feed.rss') }

    before(:each) do
      allow(RSS::Parser).to receive(:parse)
        .with(OmSwami::Quotes::Feed::URL, false)
        .and_return RSS::Parser.parse(sample_response, false)
    end

    it 'should save todays quote to database' do
      described_class.execute
      expect(Quote.count).to eq(1)
      expect(Quote.last.date).to eq Date.current
    end

    it 'should not duplicate the already saved blog post' do
      described_class.execute
      described_class.execute
      expect(Quote.count).to eq(1)
    end
  end
end
