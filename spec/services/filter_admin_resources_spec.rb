# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe FilterAdminResources do
  let(:admin_namespace) { Administrate::Namespace.new('admin') }
  let(:subject) { described_class.new(admin_namespace) }

  describe '#execute' do
    before do
      @dashbord_links = subject.execute.map(&:resource)
    end
    it 'should not include hidden dashboard links' do
      links = @dashbord_links - described_class::HIDDEN_PAGES
      expect(links).to eq @dashbord_links
    end
  end
end
