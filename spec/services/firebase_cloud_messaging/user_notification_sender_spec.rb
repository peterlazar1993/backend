module FirebaseCloudMessaging
  RSpec.describe UserNotificationSender do
    context 'constants' do
      describe 'priority' do
        it 'should be high' do
          expect(described_class::PRIORITY).to eq 'high'
        end
      end

      describe 'sound' do
        it 'should be default' do
          expect(described_class::SOUND).to eq 'default'
        end
      end

      describe '#execute' do
        it 'should send push notification via FCM' do
          expect_any_instance_of(FCM).to receive(:send_to_topic)
          described_class.execute('Message')
        end
      end
    end
  end
end
