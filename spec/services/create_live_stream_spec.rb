# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe CreateLiveStream do
  let(:title) { 'title' }
  let(:part) { 'id,snippet,status' }
  let(:subject) { described_class.new(title) }
  let(:namespace) { described_class::YT }

  include_context 'stub youtube api'

  describe '#execute' do
    it 'should create broadcast in youtube' do
      expect_any_instance_of(namespace::YouTubeService).
        to receive(:insert_live_stream)
      subject.execute
    end
  end

  describe 'constants' do
    it 'should have resolution 720p' do
      expect(described_class::CDN_FORMAT).to eq '720p'
    end
  end
end
