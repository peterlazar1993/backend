# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe UpdateVideoCategory do
  let(:snippet) { double('snippet', title: 'abc', description: 'abc', )}
  let(:resource) { double('video', id: 'abc', snippet: snippet) }
  let(:namespace) { described_class::YT }

  include_context 'stub youtube api'

  describe '#execute' do
    it 'should update broadcast data in youtube' do
      expect_any_instance_of(namespace::YouTubeService).
        to receive(:update_video)
      described_class.execute(resource)
    end
  end

  context 'category' do
    it 'should be Nonprofits & activism' do
      expect(described_class::BROADCAST_CATEGORY).to be 29
    end
  end
end
