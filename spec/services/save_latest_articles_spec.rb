# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaveLatestArticles do
  describe '#execute' do
    let(:sample_response) { File.read('spec/data/om_swami_feed.rss'); }

    before(:each) do
      allow(RSS::Parser).to receive(:parse)
        .with(OmSwami::Blog::Feed::URL)
        .and_return RSS::Parser.parse(sample_response)
      stub_fcm_notifications
    end

    it 'should save latest blog post to database' do
      described_class.execute
      expect(Article.count).to eq(10)
    end

    it 'should not duplicate the already saved blog post' do
      described_class.execute
      described_class.execute
      expect(Article.count).to eq(10)
    end

    describe 'error notifications' do
      let(:inavalid_artice) do
        [{ title: nil, cover_image: nil }]
      end

      before do
        allow_any_instance_of(OmSwami::Blog::Feed).to receive(:fetch).
          and_return inavalid_artice
      end
      it 'should send error notification' do
        expect { described_class.execute }.to change {
          ActionMailer::Base.deliveries.count
        }.by(1)
      end
    end
  end
end
