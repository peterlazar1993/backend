# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe CreateBroadcast do
  let(:title) { 'title' }
  let(:description) { 'description' }
  let(:start_time) { 'start time'}
  let(:part) { 'id,snippet,status' }
  let(:livestream) { 'sdsds' }
  let(:subject) { described_class.new(title, description, start_time, livestream) }
  let(:namespace) { described_class::YT }

  include_context 'stub youtube api'

  describe '#execute' do
    it 'should create broadcast in youtube' do
      expect_any_instance_of(namespace::YouTubeService).
        to receive(:insert_live_broadcast)
      subject.execute
    end
  end

  context 'broadcast privacy status' do
    it 'should be unlisted' do
      expect(described_class::BROADCAST_PRIVACY).to be 'unlisted'
    end
  end

  context 'autostart configuration' do
    let(:settings) { subject.send(:metadata)[:content_details] }
    it 'should be set to true' do
      expect(settings[:enable_auto_start]).to be true
    end
  end
end
