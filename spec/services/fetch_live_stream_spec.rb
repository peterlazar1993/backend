# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

RSpec.describe FetchLiveStream do
  let(:id) { 'Qzhm5VxYqz0' }
  let(:part) { 'id,snippet,status' }
  let(:subject) { described_class.new(id) }
  let(:namespace) { described_class::YT }

  include_context 'stub youtube api'

  describe '#execute' do
    it 'should fetch broadcast data from youtube' do
      expect_any_instance_of(namespace::YouTubeService).
        to receive(:list_live_streams)
      subject.execute
    end
  end
end
