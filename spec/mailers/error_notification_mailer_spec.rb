require "rails_helper"

RSpec.describe ErrorNotificationMailer, type: :mailer do
  describe 'notify' do
    let(:error_class) { SaveLatestBooks.to_s }
    let(:error_msg) { "Cover image can not be blank" }
    let(:default_to) { ['vaidyanathan.b@multunus.com'] }
    let(:subject_line) { 'Alert - An error occured in production' }
    let(:mail) { ErrorNotificationMailer.notify(error_class, error_msg) }

    it "renders the headers" do
      expect(mail.subject).to eq subject_line
      expect(mail.to).to eq default_to
    end

    it 'should assign @klass and @error' do
      expect(mail.body.encoded).to match(error_class)
      expect(mail.body.encoded).to match(error_msg)
    end
  end

end
