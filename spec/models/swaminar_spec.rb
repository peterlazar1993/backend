require 'rails_helper'
require 'spec_helper'
require 'youtube_api'
require 'google/apis/youtube_v3'

RSpec.describe Swaminar, type: :model do
  let(:slug) { 'xHi2lsi' }
  let(:swaminar) { Swaminar.new(broadcast_slug: slug) }
  let(:base) { 'https://www.youtube.com/watch?v=' }

  include_context 'stub youtube api'

  describe 'validations' do
    it { should validate_presence_of(:broadcast_slug) }
  end

  describe 'associations' do
    it { should have_many(:attendees) }
    it { should have_many(:questions) }
  end

  describe 'constants' do
    it 'should be correct' do
      expect(Swaminar::YOUTUBE_BASE).to eq base
    end
  end

  context 'youtube data' do
    it 'should return correct data from youtube' do
      expect(swaminar.title).to eq title
      expect(swaminar.description).to eq description
      expect(swaminar.scheduled_at).to eq start_time
    end
  end
end
