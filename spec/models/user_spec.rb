require 'rails_helper'

RSpec.describe User, type: :model do
  it {should have_many(:devices)}
  it {should have_many(:sessions)}
  it {should have_many(:favorites)}
  it {should have_many(:favorited_private_meditations)}
end
