require 'rails_helper'

RSpec.describe Gallery, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:link) }
  it { should validate_presence_of(:cover_image) }

  it { should have_many(:gallery_images) }
end
