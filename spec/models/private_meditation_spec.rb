require 'rails_helper'

RSpec.describe PrivateMeditation, type: :model do
  it {should belong_to(:category).counter_cache(:count_of_meditations)}
  it { should have_attached_file(:guided_track) }
  it { should have_attached_file(:music_track) }
  it { should have_many(:favorites) }

  it { should validate_attachment_content_type(:guided_track).
                allowing('audio/mpeg','audio/mp3')}
  it { should validate_attachment_content_type(:music_track).
                allowing('audio/mpeg','audio/mp3')}

  it "Notify users" do
    expect(PrivateMeditation.new).to respond_to(:notify_users)
  end

  describe "Guided track Duration" do
    it "Update if track is uploaded" do
      private_meditation = create(:private_meditation_with_guided_track)

      expect(private_meditation.guided_track_duration_in_minutes.to_i).to eq 5
    end

    it "Nil if no track available" do
      private_meditation =
        create(:private_meditation_with_guided_track,guided_track: nil)

      expect(private_meditation.guided_track_duration_in_minutes).to be nil
    end
  end

  describe "Music track Duration" do
    it "Update if track is uploaded" do
      private_meditation = create(:private_meditation_with_music_track)
      expect(private_meditation.music_track_duration_in_minutes.to_i).to eq 4
    end

    it "Nil if no track available" do
      private_meditation =
        create(:private_meditation_with_music_track,music_track: nil)

      expect(private_meditation.music_track_duration_in_minutes).to be nil
    end
  end

  describe "Meditation duration" do
    it "Same as guided meditation duration if music meditation is not present" do
      private_meditation = create(:private_meditation_with_guided_track)
      expect(private_meditation.duration_in_minutes).
        to eq private_meditation.guided_track_duration_in_minutes
    end

    it "Same as music meditation if guided meditation is not present" do
      private_meditation = create(:private_meditation_with_music_track)
      expect(private_meditation.duration_in_minutes).
        to eq private_meditation.music_track_duration_in_minutes
    end

    it "Same as guided track if both are present" do
      private_meditation = create(:private_meditation_with_both_tracks)
      expect(private_meditation.duration_in_minutes).
        to eq private_meditation.guided_track_duration_in_minutes
    end

  end

  describe "Update" do
    it "If tracks are not updated, don't update the duration " do
      private_meditation = create(:private_meditation_with_both_tracks)

      private_meditation.update_attributes({guided_track_description: "Guided by Om Swami",
        music_track_description: "Soothing flute"})
      private_meditation = PrivateMeditation.last
      expect(private_meditation.duration_in_minutes).not_to be nil
      expect(private_meditation.guided_track_duration_in_minutes).not_to be nil
      expect(private_meditation.music_track_duration_in_minutes).not_to be nil
    end
  end
end
