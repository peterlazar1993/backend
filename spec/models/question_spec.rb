require 'rails_helper'

RSpec.describe Question, type: :model do
  let(:question) { Question.new(body: 'Question body') }
  let(:user) { stub_model(User, name: 'John Doe') }

  context 'profanity check' do
    let(:shit_question) do
      Question.new(body: 'text with shit', user: user)
    end
    let(:good_question) do
      Question.new(body: 'No vulgur slang', user: user)
    end

    it 'should check profanity of question body' do
      expect(shit_question).not_to be_valid
      expect(good_question).to be_valid
    end
  end

  describe '#votes_count' do
    before do
      allow(question).to receive(:votes_for) { double('Votes', size: 3) }
    end

    it 'should return correct upvotes count' do
      expect(question.votes_count).to eq 3
    end
  end

  describe '#asked_by' do
    before do
      allow(question).to receive(:user) { user }
    end

    it 'should return who asked the question' do
      expect(question.asked_by).to eq 'John Doe'
    end
  end
end
