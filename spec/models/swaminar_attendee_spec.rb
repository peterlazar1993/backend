require 'rails_helper'

RSpec.describe SwaminarAttendee, type: :model do
  describe 'associations' do
    it { should belong_to(:swaminar) }
    it { should belong_to(:user) }
  end
end
