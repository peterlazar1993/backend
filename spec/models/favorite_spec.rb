require 'rails_helper'

RSpec.describe Favorite, type: :model do
  it { is_expected.to belong_to(:favorable) }
  it { is_expected.to belong_to(:user) }

  %i[favorable user].each do |attribute|
    it { is_expected.to validate_presence_of attribute }
  end

end
