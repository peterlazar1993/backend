require 'rails_helper'

RSpec.describe Session, type: :model do
  it{should validate_presence_of(:played_at)}
  it{should validate_presence_of(:meditation_type)}
  it{should validate_presence_of(:duration_in_minutes)}
  it{should have_one(:feedback)}
end
