require 'rails_helper'

RSpec.describe Category, type: :model do
  it {should have_many(:private_meditations)}

  it "All category" do
    all_category  = Category.all_category
    expect(all_category.id).to eq Category::ALL_CATEGORY_ID
    expect(all_category.name).to eq Category::ALL_CATEGORY_NAME
  end
end
