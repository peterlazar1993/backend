require 'rails_helper'

RSpec.describe Device, type: :model do
  it { should validate_presence_of(:device_id)}
  it { should validate_presence_of(:platform)}
  it { should validate_presence_of(:push_token)}
  it {should belong_to(:user)}

  it "Generate unique Identification" do
    device = create(:device, uid: nil)
    expect(device.uid).not_to be nil
  end
end
