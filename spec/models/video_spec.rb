require 'rails_helper'

RSpec.describe Video, type: :model do
  it { is_expected.to belong_to :video_category }
  it { is_expected.to have_many(:favorites) }

  it { is_expected.to validate_uniqueness_of :youtube_video_id }
  %i[title description youtube_video_id thumbnail_url published_at video_category].each do |attribute|
    it { is_expected.to validate_presence_of attribute }
  end

  describe 'scopes' do
    include_context 'videos in multiple categories'

    describe '.latest_first' do
      it 'fetches videos with last published first' do
        videos = Video.latest_first
        expect(videos.first.published_at).to be > videos.second.published_at
      end
    end

    describe '.of_category' do
      it 'returns videos of the given video category' do
        expect(Video.of_category(video_category1.id)).not_to include video2
      end

      it 'returns all videos if no category is given' do
        expect(Video.of_category(nil)).to include video1, video2
      end
    end
  end

  describe '.latest' do
    it 'returns the last published video' do
      video_category = create :video_category
      video_published_earlier = build :video, published_at: Time.now - 1.hour, video_category: video_category
      video_published_now = build :video, published_at: Time.now, video_category: video_category
      video_published_later = build :video, published_at: Time.now, video_category: video_category
      [video_published_now, video_published_later, video_published_earlier].map(&:save)

      expect(Video.latest).to eql video_published_later
    end
  end

  describe "#favorited_by_user?" do
    let(:user) { build :user }
    let(:video) { create :video_with_category }

    it "returns false if the video was not favorited by the iven user" do
      expect(video.favorited_by_user?(user)).to be false
    end

    it "returns true if the video was favorited by the iven user" do
      create :favorite, user: user, favorable: video

      expect(video.favorited_by_user?(user)).to be true
    end
  end
end
