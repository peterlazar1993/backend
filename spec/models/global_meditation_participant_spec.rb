require 'rails_helper'

RSpec.describe GlobalMeditationParticipant, type: :model do

  before(:all) do
    Geocoder.configure(:lookup => :test)

    Geocoder::Lookup::Test.add_stub(
    [20.593684,78.96288], [
      {
        'latitude'     => 20.593684,
        'longitude'    => 78.96288,
        'city'      => 'Bangalore',
        'state'        => 'Karnataka',
        'state_code'   => '',
        'country'      => 'India',
        'country_code' => 'IN'
      }
     ]
    )

    Geocoder::Lookup::Test.add_stub(
    "Bangalore, Karnataka, IN", [
      {
        'latitude'     => 20.593684,
        'longitude'    => 78.96288,
        'city'      => 'Bangalore',
        'state'        => 'Karnataka',
        'state_code'   => '',
        'country'      => 'India',
        'country_code' => 'IN'
      }
     ]
    )
  end

  it {should belong_to(:user)}
  it {should belong_to(:global_meditation).counter_cache(:joined_meditator_count)}

  it "Validation for valid user and global meditation" do
    member = GlobalMeditationParticipant.create
    expect(member.errors.keys.include?(:user)).to be true
    expect(member.errors.keys.include?(:global_meditation)).to be true
    expect(member.valid?).to be false
  end

  it "Update address of the meditator" do
    participant = GlobalMeditationParticipant.create(user: User.create,
        global_meditation: create(:global_meditation),
        ip_address: "106.51.18.248",
        latitude: 20.593684,
        longitude: 78.96288
    )
    expect(participant.location).not_to be_nil
  end

  # it "Notify other meditators when someone new joins" do
  #   expect_any_instance_of(GlobalMeditationParticipant).to receive(:notify_other_meditators)
  #   GlobalMeditationParticipant.create(
  #     user: User.new,
  #     global_meditation: create(:global_meditation)
  #   )
  # end
end
