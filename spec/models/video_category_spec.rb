require "rails_helper"

RSpec.describe VideoCategory, type: :model do
  it { is_expected.to validate_uniqueness_of :youtube_channel_id }
end
