require 'rails_helper'

RSpec.describe Article, type: :model do
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:summary) }
  it { should validate_presence_of(:cover_image) }
  it { should validate_presence_of(:published_at) }
  it { should validate_presence_of(:link) }
end
