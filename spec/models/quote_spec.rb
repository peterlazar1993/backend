require 'rails_helper'

RSpec.describe Quote, type: :model do
  it { should validate_presence_of(:content) }
  it { should validate_presence_of(:date) }

  it { should validate_uniqueness_of(:content).scoped_to(:date) }
end
