require 'rails_helper'

RSpec.describe SessionFeedback, type: :model do
  it {should belong_to(:session)}
  it {should validate_presence_of(:session)}
  it {should validate_presence_of(:ratings)}
end
