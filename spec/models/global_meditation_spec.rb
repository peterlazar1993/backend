require 'rails_helper'

RSpec.describe GlobalMeditation, type: :model do
  it { should validate_presence_of(:scheduled_at)}
  it { should validate_presence_of(:track)}
  it { should have_attached_file(:track) }

  it { should validate_attachment_content_type(:track).
                allowing('audio/mpeg','audio/mp3')}

  it "Find the track duration from the uploaded file" do
    global_meditation = create(:global_meditation)
    expect(global_meditation.duration_in_minutes.to_i).to eq 4
  end

  it "Notify users" do
    expect(GlobalMeditation.new).to respond_to(:notify_users)
  end

end
