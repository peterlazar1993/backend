require 'rails_helper'

RSpec.describe 'Video categories API', type: :request do

  it '/video_categories returns all video categories' do
    video_category1 = create :video_category
    http_login
    get '/video_categories', params: {}, headers: @env

    response_obj = JSON.parse(response.body)
    expect(response_obj.size).to eql 1

    expect(response_obj[0]['id']).to eql video_category1.id
    expect(response_obj[0]['name']).to eql video_category1.name
    expect(response_obj[0]['description']).to eql video_category1.description
  end

end
