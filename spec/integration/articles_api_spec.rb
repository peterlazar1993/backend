# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Articles API', type: :request do

  include_context 'secure end-point test', '/articles'

  context 'with valid device token' do
    it '/articles should return latest 10 articles' do
      stub_fcm_notifications
      create_list :article, 11
      http_login
      get '/articles', params: {}, headers: @env

      response_obj = JSON.parse(response.body)
      expect(response_obj.size).to eql 10
    end
  end
end
