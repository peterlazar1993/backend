# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Custom error handling', type: :request do
  before do
    http_login
    stub_fcm_notifications
    @user = create(:user)
    @user.confirm
    @auth_headers = @user.create_new_auth_token
    @swaminar = create(:swaminar)
  end

  context 'RecordNotFound error' do
    it 'should have custom error response' do
      # calling swaminar_questions_path('n')
      # should raise ActiveRecord::RecordNotFound error
      get swaminar_questions_path('n'), headers: @auth_headers
      response_body = JSON.parse(response.body)
      expect(response_body['status']).to eq 404
      expect(response_body['error']).to eq 'record_not_found'
      expect(response_body['message']).to eq "Couldn't find Swaminar with 'id'=n"
    end
  end

  context 'StandardError' do
    before do
      @err_msg = 'My Error'
      expect_any_instance_of(User).to receive(:get_up_voted) do
        raise StandardError, @err_msg
      end
    end

    it 'should have custom error response' do
      get swaminar_questions_path(@swaminar), headers: @auth_headers
      response_body = JSON.parse(response.body)
      expect(response_body['status']).to eq 500
      expect(response_body['error']).to eq 'standard_error'
      expect(response_body['message']).to eq @err_msg
    end
  end

  context 'CustomError' do
    before do
      @error = 'custom_error'
      @status = 500
      @msg = 'My Error'
      expect_any_instance_of(User).to receive(:get_up_voted) do
        raise Error::CustomError.new(@error, @status, @msg)
      end
    end

    it 'should have custom error response' do
      get swaminar_questions_path(@swaminar), headers: @auth_headers
      response_body = JSON.parse(response.body)
      expect(response_body['status']).to eq @status
      expect(response_body['error']).to eq @error
      expect(response_body['message']).to eq @msg
    end
  end
end
