# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Books API', type: :request do

  include_context 'secure end-point test', '/books'

  context 'with valid device token' do
    it '/books should return latest 10 books' do
      stub_fcm_notifications
      create_list :book, 11
      http_login
      get '/books', params: {}, headers: @env

      response_obj = JSON.parse(response.body)
      expect(response_obj.size).to eql 10
    end
  end
end
