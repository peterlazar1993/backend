# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Questions API', type: :request do
  before do
    stub_fcm_notifications
    @question = create :swaminar_question
    @question.user.confirm
    @auth_headers = @question.user.create_new_auth_token
  end

  let(:question_response) do
    {
      'id' => @question.id,
      'body' => @question.body,
      'asked_by' => @question.asked_by,
      'votes_count' => @question.get_upvotes.size
    }
  end

  it 'get /:swaminar_id/questions returns questions' do
    get swaminar_questions_path(@question.askable), headers: @auth_headers
    api_response = JSON.parse(response.body)
    expect(api_response.keys).to eq %w[questions user_questions user_upvoted]
    expect(api_response['questions']).to eq [question_response]
  end

  it 'post /:swaminar_id/questions/:question_id upvotes question' do
    post upvote_swaminar_question_path(@question.askable, @question),
         headers: @auth_headers
    expect(JSON.parse(response.body)['upvotes']).to eq 1
  end

  it 'post /:swaminar_id/questions/:question_id undo vote' do
    post undo_vote_swaminar_question_path(@question.askable, @question),
         headers: @auth_headers
    expect(JSON.parse(response.body)['upvotes']).to eq 0
  end
end
