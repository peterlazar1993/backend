# frozen_string_literal: true

require 'rails_helper'
require 'youtube_api/client'

RSpec.describe 'Populate Youtube Data Job' do
  let!(:category_1) { create :video_category }
  let!(:category_2) { create :video_category }

  let(:channel_1_response_json_page_1) { file_fixture('youtube_api_response/channel_1_page_1.json').read }
  let(:channel_1_response_json_page_2) { file_fixture('youtube_api_response/channel_1_page_2.json').read }
  let(:channel_2_response_json_page_1) { file_fixture('youtube_api_response/channel_2_page_1.json').read }
  let(:response_with_existing_video) { file_fixture('youtube_api_response/response_with_bad_data.json').read }

  let(:youtube_api_url) { 'https://www.googleapis.com/youtube/v3/search' }

  context 'in case of valid video data' do
    before do
      allow(RestClient).to receive(:get)
      .with(youtube_api_url, params: hash_including(channelId: category_1.youtube_channel_id))
      .and_return(channel_1_response_json_page_1, channel_1_response_json_page_2)

      allow(RestClient).to receive(:get)
      .with(youtube_api_url, params: hash_including(channelId: category_2.youtube_channel_id))
      .and_return(channel_2_response_json_page_1)
    end

    it 'populates the Video table for each category' do
      PopulateYoutubeDataJob.execute

      expect(Video.of_category(category_1.id).count).to eql 4
      expect(Video.of_category(category_2.id).count).to eql 1
    end
  end

  context 'in case the response has existing video data' do
    before do
      allow(RestClient).to receive(:get)
      .with(youtube_api_url, params: hash_including(channelId: category_1.youtube_channel_id))
      .and_return(channel_1_response_json_page_1, response_with_existing_video, channel_1_response_json_page_2)

      allow(RestClient).to receive(:get)
      .with(youtube_api_url, params: hash_including(channelId: category_2.youtube_channel_id))
      .and_return(channel_2_response_json_page_1)
    end

    it "doesn't stop further insertion" do
      expect { PopulateYoutubeDataJob.execute }.not_to raise_error

      expect(Video.of_category(category_1.id).count).to eql 5
      expect(Video.of_category(category_2.id).count).to eql 1
    end
  end
end
