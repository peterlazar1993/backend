require "rails_helper"
require "pry"

RSpec.describe "Videos API", type: :request do
  context 'with a valid device' do
    before do
      http_login
    end

    describe "GET /videos" do
      include_context "videos in multiple categories"

      let(:videos) { [video1, video2].sort {|a,b| b.published_at <=> a.published_at} }
      let(:user) { create :user }

      it "returns all videos with the latest first" do
        videos.each do |video|
          create :favorite, favorable: video, user: create(:user)
        end
        get "/videos", params: {}, headers: @env
        response_obj = JSON.parse(response.body)

        expect(response).to be_success
        expect(response_obj.size).to eql 2
        videos.each_with_index do |video, index|
          expect(response_obj[index]["id"]).to eql video.id
          expect(response_obj[index]["title"]).to eql video.title
          expect(response_obj[index]["description"]).to eql video.description
          expect(response_obj[index]["youtube_video_id"]).to eql video.youtube_video_id
          expect(response_obj[index]["thumbnail_url"]).to eql video.thumbnail_url
          expect(response_obj[index]["published_at"]).to eql video.published_at.iso8601.to_s
          expect(response_obj[index]["favorites_count"]).to eql 1
        end
      end

      context "for an authenticated user" do
        it "includes the user's favorited status for each video" do
          create :favorite, user: user, favorable: video1
          get "/videos", headers: @env.merge(user.create_new_auth_token)
          response_obj = JSON.parse response.body

          expect(response).to be_success
          expect(response_obj[1]["is_favorited"]).to be true
          expect(response_obj[0]["is_favorited"]).to be false
        end
      end

      context "for a non authenticated user" do
        it "includes the favorited status as false for each video" do
          create :favorite, user: user, favorable: video1
          get "/videos", params: {}, headers: @env
          response_obj = JSON.parse response.body

          expect(response).to be_success
          expect(response_obj[1]["is_favorited"]).to be false
          expect(response_obj[0]["is_favorited"]).to be false
        end
      end

      it "includes pagination links in the 'Link' HTTP header" do
        2.times { create :video, video_category: video_category1 }
        get "/videos?per_page=1&page=2", params: {}, headers: @env

        expect(response).to be_success
        expect(response.headers["Link"]).to include "<http://www.example.com/videos?page=1&per_page=1>; rel=\"prev\""
        expect(response.headers["Link"]).to include "<http://www.example.com/videos?page=3&per_page=1>; rel=\"next\""
      end
    end

    describe "GET /videos?video_category_id=<CATEGORY_ID>" do
      include_context "videos in multiple categories"

      it "returns videos of a particular category" do
        get "/videos?video_category_id=#{video_category1.id}", params: {}, headers: @env
        response_obj = JSON.parse(response.body)

        expect(response).to be_success
        expect(response_obj.size).to eql 1
        expect(response_obj[0]["id"]).to eql video1.id
      end
    end

    describe "POST /videos/:id/favorite" do
      let(:user) { create(:user) }
      let(:video) { (create :video_with_category) }

      it "marks the video as favorited by the user" do
        expect do
          post "/videos/#{video.id}/favorite", params: {}, headers: @env.merge(user.create_new_auth_token)
        end.to change { video.favorites.count }.from(0).to(1)

        expect(response).to be_created
        expect(video.favorites.first.user).to eql user
      end

      it "behaves well if the video was already favorited by the user" do
        post "/videos/#{video.id}/favorite", params: {}, headers: @env.merge(user.create_new_auth_token)

        expect do
          post "/videos/#{video.id}/favorite",params: {}, headers: @env.merge(user.create_new_auth_token)
        end.to_not change { video.favorites.count }

        expect(response).to be_ok
      end

      it "is not accessible without user authentication" do
        video = create :video_with_category
        post "/videos/#{video.id}/favorite", params: {}, headers: @env

        expect(response).to be_unauthorized
      end
    end

    describe "POST /videos/:id/unfavorite" do
      let(:user) { create(:user) }
      let(:video) { create(:video_with_category) }

      it "removes the video favoriting by the user" do
        post "/videos/#{video.id}/favorite", headers: @env.merge(user.create_new_auth_token)

        expect do
          post "/videos/#{video.id}/unfavorite", headers: @env.merge(user.create_new_auth_token)
        end.to change { video.favorites.count }.from(1).to(0)

        expect(response).to be_ok
      end

      it "behaves well if the video was already unfavorited by the user" do
        expect do
          post "/videos/#{video.id}/unfavorite", headers: @env.merge(user.create_new_auth_token)
        end.to_not change { video.favorites.count }

        expect(response).to be_ok
      end

      it "is not accessible without user authentication" do
        video = create :video_with_category

        post "/videos/#{video.id}/unfavorite", params: {}, headers: @env

        expect(response).to be_unauthorized
      end
    end

    describe "GET /videos?filter_type=<FILTER_TYPE>" do
      include_context "videos in multiple categories"

      let(:user) { create(:user) }
      let(:video) { create(:video_with_category) }


      it "returns videos favorited by the user" do
        post "/videos/#{video.id}/favorite", params: {}, headers: @env.merge(user.create_new_auth_token)

        get "/videos?filter_type=favorites&video_category_id=#{video.video_category_id}", headers: @env.merge(user.create_new_auth_token)
        response_obj = JSON.parse(response.body)

        expect(response).to be_success
        expect(response_obj.size).to eql 1
        expect(response_obj[0]["id"]).to eql video.id
      end
    end
  end
end
