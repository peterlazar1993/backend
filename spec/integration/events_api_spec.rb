# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Events API', type: :request do

  include_context 'secure end-point test', '/events'

  before do
    stub_fcm_notifications
    http_login
  end
  context 'with valid device token' do
    before do
      @elapsed_event = create(:event, scheduled_at: 2.days.ago)
      @upcoming_event = create(:event, scheduled_at: 3.days.from_now)
    end

    it '/events should return only upcoming events' do
      get '/events', params: {}, headers: @env
      response_obj = JSON.parse(response.body)
      expect(response_obj.size).to eql 1
      expect(response_obj.first).to include('title' => @upcoming_event.title)
      expect(response_obj.first).not_to include('title' => @elapsed_event.title)
    end

    it 'response should be sorted in the ascending order of scheduled_at' do
      latest_event = create(:event, scheduled_at: 1.month.from_now)
      get '/events', params: {}, headers: @env
      response_obj = JSON.parse(response.body)
      expect(response_obj.size).to eql 2
      expect(response_obj.first['title']).to eq @upcoming_event.title
    end

    it 'response should be sorted in descending order of scheduled_at if the query param order_by=DESC is present' do
      latest_event = create(:event, scheduled_at: 2.month.from_now)
      get '/events', params: {order_by:"DESC"}, headers: @env
      response_obj = JSON.parse(response.body)
      expect(response_obj.size).to eql 2
      expect(response_obj.first['title']).to eq latest_event.title
    end
  end
end
