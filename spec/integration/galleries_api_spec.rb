# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Galleries API', type: :request do
  include_context 'secure end-point test', '/galleries'

  before do
    stub_fcm_notifications
    http_login
  end

  context 'with valid device token' do
    it 'should have correct pagination' do
      create_list :gallery, 11
      get '/galleries', params: {}, headers: @env
      response_obj = JSON.parse(response.body)
      expect(response_obj.size).to eql 10
    end

    it 'response should have album size' do
      gallery = create :gallery_with_images
      get '/galleries', params: {}, headers: @env
      response_obj = JSON.parse(response.body)
      expect(response_obj.first['album_size']).to eq gallery.gallery_images.count
    end
  end
end
