# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User stats API', type: :request do

  describe 'user stats' do
    before do
      @user = create(:user)
      @user.confirm
      @auth_headers = @user.create_new_auth_token

      @day1 = 7.days.ago
      @day2 = 6.days.ago
      @day3 = 5.days.ago
      # streak 1 : longest
      create(:global_meditation_session, user: @user, played_at: @day1)
      create(:global_meditation_session, user: @user, played_at: @day2)
      create(:global_meditation_session, user: @user, played_at: @day3)
    end

    context 'longest streek ' do
      it 'should show have correct streak info' do
        get users_stats_path, headers: @auth_headers
        response_body = JSON.parse(response.body)
        expect(response_body['sessions'].count).to eq 3
        stats = response_body['session_stats']
        expect(stats['longest_streak_in_days']).to eq 3
        expect(stats['longest_streak_start_date']).to eq @day1.strftime('%Y-%m-%d')
        expect(stats['longest_streak_end_date']).to eq @day3.strftime('%Y-%m-%d')
        expect(stats['current_streak']).to eq 0
        expect(stats['current_streak_start_date']).to eq Date.tomorrow.strftime('%Y-%m-%d')
        expect(stats['current_streak_end_date']).to eq Date.current.strftime('%Y-%m-%d')
      end
    end

    context 'current streek' do
      before do
        @new_day1 = 1.day.ago
        # streak 2 : latest
        @session1 = create(:global_meditation_session, user: @user, played_at: @new_day1)
        @session2 = create(:global_meditation_session, user: @user, played_at: Time.current)
      end


      it 'should have correct streak info' do
        get users_stats_path, headers: @auth_headers
        response_body = JSON.parse(response.body)
        expect(response_body['sessions'].count).to eq 5
        stats = response_body['session_stats']
        expect(stats['current_streak']).to eq 2
        expect(stats['current_streak_start_date']).to eq @new_day1.strftime('%Y-%m-%d')
        expect(stats['current_streak_end_date']).to eq Date.current.strftime('%Y-%m-%d')
      end
    end
  end
end
