# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GalleryImages API', type: :request do
  include_context 'secure end-point test', '/galleries'

  context 'with valid device token' do
    before do
      stub_fcm_notifications
      @gallery = create(:gallery_with_images)
      http_login
    end

    it 'should return gallery images of galley' do
      get '/gallery_images', params: { id: @gallery.id }, headers: @env
      expect(response).to be_success
      @response_obj = JSON.parse(response.body)
      expect(@response_obj.size).to eql @gallery.gallery_images.count
    end
  end
end
