# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Swaminar Questions API', type: :request do
  include Warden::Test::Helpers
  before :all do
    stub_fcm_notifications
    @swaminar = create(:swaminar_with_attendees, attendees_count: 1)
    @user = @swaminar.attendees.first.user
    @unauthorized_user = create(:user)
  end

  after(:all) do
    DatabaseCleaner.clean_with(:truncation)
  end

  describe 'create questions' do
    before do
      @user.confirm
      @auth_headers = @user.create_new_auth_token
    end
    context 'with valid attributes' do
      it 'should create question' do
        post swaminar_questions_path(@swaminar),
             params: { question: { body: 'Viola?' } },
             headers: @auth_headers
        expect(response).to have_http_status(:created)
      end
    end

    context 'with profane language' do
      before do
        post swaminar_questions_path(@swaminar),
             params: { question: { body: 'Shit question?' } },
             headers: @auth_headers
        @response = JSON.parse(response.body)
      end

      it 'should not create question' do
        expect(@response['errors']['body']).to eq [I18n.t('question.profanity_error')]
      end
    end

    context 'with invalid attributes' do
      before do
        post swaminar_questions_path(@swaminar),
             params: { question: { body: nil } },
             headers: @auth_headers
        @response = JSON.parse(response.body)
      end

      it 'should not create question' do
        expect(@response['errors']['body']).to eq [I18n.t('question.presence_error')]
      end
    end
  end

  describe 'updating questions' do
    before :all do
      @question = create(:swaminar_question, askable: @swaminar, user: @user)
    end
    context "user trying to edit someone else's question" do
      before do
        @unauthorized_user.confirm
        @auth_headers = @unauthorized_user.create_new_auth_token
        put swaminar_question_path(@swaminar, @question),
             params: { question: { body: 'Not my question' } },
             headers: @auth_headers
      end

      it 'should return forbidden response' do
        expect(response).to have_http_status(:forbidden)
        response_body = JSON.parse(response.body)
        expect(response_body['status']).to eq 403
        expect(response_body['error']).to eq 'authorization_error'
        expect(response_body['message']).to eq I18n.t('errors.pundit_errror')
      end
    end
  end
end
