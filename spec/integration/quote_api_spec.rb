# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Quotes API', type: :request do

  include_context 'secure end-point test', '/quotes'

  context 'with valid device token' do
    it '/quotes should return latest quotes' do
      create_list :quote, 1
      http_login
      get '/quotes', params: {}, headers: @env

      response_obj = JSON.parse(response.body)
      expect(response_obj.size).to eql 1
    end
  end
end
